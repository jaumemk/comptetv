/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!********************************!*\
  !*** ./resources/js/shared.js ***!
  \********************************/
$(".burger").on('click', function (e) {
  e.preventDefault();
  oh = $("#navigation-wrapper").toggleClass('active');
});
/*
    Scroll
*/

var lst = 0;
$(window).on('scroll', function () {
  st = $(window).scrollTop();

  if (st > 20) {
    $("#navigation").addClass('bg-white');
  } else {
    $("#navigation").removeClass('bg-white');
  }

  if (st > lst || st < 20) {
    // downscroll code
    $('#aside-info').removeClass('popt');
  } else {
    // upscroll code
    $('#aside-info').addClass('popt');
  }

  lst = st;
});
/*
    Events
*/

$(".a-scroll-to").on('click', function (e) {
  e.preventDefault();
  href = $(this).attr('href');
  oh = $("#navigation").outerHeight() - 2;
  $('html, body').animate({
    scrollTop: $(href).offset().top - oh
  }, 2000);
});
/******/ })()
;