<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'gallery' => 'array',
        'date' => 'datetime',
    ];

    /**
     * The "booted" method of this model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('ordered', function (Builder $builder) {
            $builder->orderBy('weight', 'desc')->orderBy('date', 'desc');
        });
    }

    /**
     * Scope a query to only include active projects.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
     * Scope a query to only include afeatured projects.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    /**
     * Get the project's first image / aka thumbnail.
     *
     * @param  string  $value
     * @return string
     */
    public function getThumbnailAttribute()
    {
        return $this->gallery[0];
    }

    /**
     * Get the project's first image / aka thumbnail.
     *
     * @param  string  $value
     * @return string
     */
    public function getTypeAttribute($value)
    {

        $values = [
            'Prom' => 'Promoció',
            'Part' => 'Particular',
            'Empr' => 'Empresa',
            'Op' => 'Obra Pública',
            'Refo' => 'Reforma',
        ];

        if(isset($values[$value])) {
            return $values[$value];
        }

        return 'No definit'; // undefined

    }

    /**
     * Get the projects's year.
     *
     * @return string
     */
    public function getYearAttribute()
    {
        return $this->date->format('Y');
    }
}
