let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'assets/js').setPublicPath('www');
mix.sass('resources/scss/app.scss', 'assets/css').setPublicPath('www');
mix.js('resources/js/vslider.js', 'assets/js').setPublicPath('www');

/* shared frontend */

mix.js('resources/js/shared.js', 'assets/js').setPublicPath('www');
mix.sass('resources/scss/shared.scss', 'assets/css').setPublicPath('www');

/* start legacy frontend */

mix.sass('resources/legacy/sass/main.scss', 'legacy/css').setPublicPath('www');

/* end legacy frontend */

mix.browserSync('localhost');