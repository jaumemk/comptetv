<?php

use App\Models\Project;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $projects = Project::active()->featured()->limit(4)->get();
    $title = 'Benvinguts';

    return view('pages.home', compact('title','projects'));

})->name('home');

Route::get('/home2', function () {

    $projects = Project::active()->featured()->limit(4)->get();
    $title = 'Benvinguts';

    return view('pages.home2', compact('title','projects'));

})->name('home2');

Route::get('empresa', function () {
    $title = 'Empresa';
    return view('pages.company', compact('title'));
})->name('company');

Route::get('contacte', function () {
    $title = 'Contacte';
    return view('pages.contact', compact('title'));
})->name('contact');

Route::post('contacte', function (App\Http\Requests\ContactFormRequest $request) {

    $validated = $request->validated();

    $fields = [
        "name",
        "email",
        "phone",
        "comments",
        "privacy"
    ];

    $this->data['fields'] = $request->only($fields);
    $this->data['content'] = 'Missatge des de la web de compte.tv';

    Mail::to('info@compte.tv')->send(new \App\Mail\ContactForm($this->data));

    return redirect()->back()->with('message', 'Gàrcies, ens posarem en contacte amb vosté en la major brevetat possible.');

})->name('contact');

Route::get('projectes', function () {

    $projects = Project::active()->get();
    $title = 'Projectes';
    return view('pages.projects', compact('projects', 'title'));

})->name('projects');

Route::get('promocio/{name}', function ($name) {

    $title = 'Promocions';

    if(view()->exists('promotions/'.$name)) return view('promotions/'.$name, compact('title'));

    return abort(404);

})->name('promotion');

Route::get('promocions', function () {
    return redirect(route('home'));
});

Route::get('politica-de-privacitat', function () {
    return view('pages.privacy');
})->name('privacy');

Route::get('projecte/{slug}/{id}', function ($slug, $id) {
    
    $project = Project::findOrFail($id);

    $title = $project->name;
    
    if($slug != Str::slug($project->name, '-')) {
        return redirect(route('project', [Str::slug($project->name, '-'), $project->id]), 301); // permanent redirect
    }

    $projects  = Project::active()->get();

    foreach($projects as $key => $project)
    {  
        if($project->id == $id){
            $i = $key;
            break;
        }
    }
    
    $prevProject = isset($projects[$i-1]) ? $projects[$i-1] : $projects->last();
    $nextProject = isset($projects[$i+1]) ? $projects[$i+1] : $projects->first();

    return view('pages.project', compact('project', 'title','prevProject', 'nextProject'));
})->name('project');

