<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Compte Contact Form</title>
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */
      
      /*All the styling goes here*/
      
      img,
      .email-forms img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%; 
      }

      body,
      .email-forms body {
        background-color: #f6f6f6;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%; 
      }

      table,
      .email-forms table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }

        table td,
        .email-forms table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top; 
      }

      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

      .body,
      .email-forms .body {
        background-color: #f6f6f6;
        width: 100%; 
      }

      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container,
      .email-forms .container {
        display: block;
        margin: 0 auto !important;
        /* makes it centered */
        max-width: 580px;
        padding: 10px;
        width: 580px; 
      }

      /* This should also be a block element, so that it will fill 100% of the .container */
      .content,
      .email-forms .content {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        max-width: 580px;
        padding: 10px; 
      }

      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main,
      .email-forms .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%; 
      }

      .wrapper,
      .email-forms .wrapper {
        box-sizing: border-box;
        padding: 20px; 
      }

      .content-block,
      .email-forms .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }

      .footer,
      .email-forms .footer {
        clear: both;
        margin-top: 10px;
        text-align: center;
        width: 100%; 
      }
        .footer td,
        .footer p,
        .footer span,
        .footer a,
      .email-forms .footer td,
      .email-forms .footer p,
      .email-forms .footer span,
      .email-forms .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center; 
      }

      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4,
      .email-forms h1,
      .email-forms h2,
      .email-forms h3,
      .email-forms h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px; 
      }

      h1,
      .email-forms h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
      }

      p,
      ul,
      ol,
      .email-forms p,
      .email-forms ul,
      .email-forms ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px; 
      }
        p li,
        ul li,
        ol li,
      .email-forms p li,
      .email-forms ul li,
      .email-forms ol li {
          list-style-position: inside;
          margin-left: 5px; 
      }

      a,
      .email-forms a {
        color: #3498db;
        text-decoration: underline; 
      }

      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto; 
      }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center; 
      }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize; 
      }

      .btn-primary table td {
        background-color: #3498db; 
      }

      .btn-primary a {
        background-color: #3498db;
        border-color: #3498db;
        color: #ffffff; 
      }

      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0; 
      }

      .first {
        margin-top: 0; 
      }

      .align-center {
        text-align: center; 
      }

      .align-right {
        text-align: right; 
      }

      .align-left {
        text-align: left; 
      }

      .clear {
        clear: both; 
      }

      .mt0 {
        margin-top: 0; 
      }

      .mb0 {
        margin-bottom: 0; 
      }

      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        margin: 20px 0; 
      }

      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table[class=body] h1,
      .email-forms table[class=body] h1{
          font-size: 28px !important;
          margin-bottom: 10px !important; 
        }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a,
        .email-forms table[class=body] p,
        .email-forms table[class=body] ul,
        .email-forms table[class=body] ol,
        .email-forms table[class=body] td,
        .email-forms table[class=body] span,
        .email-forms table[class=body] a, {
          font-size: 16px !important; 
        }
        table[class=body] .wrapper,
        table[class=body] .article,
        .email-forms table[class=body] .wrapper,
        .email-forms table[class=body] .article {
          padding: 10px !important; 
        }
        table[class=body] .content,
        .email-forms table[class=body] .content  {
          padding: 0 !important; 
        }
        table[class=body] .container,
        .email-forms table[class=body] .container  {
          padding: 0 !important;
          width: 100% !important; 
        }
        table[class=body] .main,
        .email-forms table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important; 
        }
        table[class=body] .btn table,
        .email-forms table[class=body] .btn table {
          width: 100% !important; 
        }
        table[class=body] .btn a,
        .email-forms table[class=body] .btn a {
          width: 100% !important; 
        }
        table[class=body] .img-responsive, 
        .email-forms table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important; 
        }
      }

    </style>
  </head>
  <body>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">
            
            <div class="align-center" style="background-color: #fff; padding: 20px 100px;">
              <img src="{{ asset('images/logo-compte.svg') }}" alt="" class="img-responsive" width="128">
            </div>

            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper">
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        @yield('content')
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->

            <!-- START FOOTER -->
            <div class="footer">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    — compte.tv &copy; 2021 —
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->

          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>