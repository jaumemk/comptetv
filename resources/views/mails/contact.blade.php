@extends('mails.layout')
@section('content')

    <h1 class="align-center">
        Formulari de contacte
    </h1>

    @isset($content)
    <div class="content-block align-center">
        {{ $content }}
    </div>
    @endisset

    @isset($fields)
        <table>
            <thead>
                <tr>
                    <th class="align-left">Camp</th>
                    <th class="align-left">Text</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($fields as $key => $value)
                    <tr>
                        <td>{{ __('mails.'.$key) }}</td>
                        <td>{{ $value }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endisset

@endsection