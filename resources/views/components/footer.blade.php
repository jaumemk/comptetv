<footer id="footer" class="text-uppercase line-height-larger text-body">
    <div class="d-flex flex-column flex-md-row justify-content-between container-fluid py-1 p-md-4">

        <div class="d-flex flex-column justify-content-between">
            <div class="text-nowrap mb-2">
                <a class="simple-hover" href="https://www.google.es/maps?q=Camprodon,+13,+08500,+Vic+Barcelona&hl=es&sll=41.934099,2.255614&sspn=0.00162,0.002798&t=w&hnear=Ronda+de+Francesc+Camprodon,+13,+08500+Vic,+Barcelona,+Catalunya&z=17" target="_blank">
                Camprodon 13<br>
                Entresòl segona<br>
                08500 Vic<br>
                Barcelona
                </a>
            </div>
            <div class="text-body">
                site by <a href="http://suki.ws" class="simple-hover" target="_blank">Suki</a>
            </div>

        </div>


        <div class="mr-auto ml-auto py-2 py-md-0 m-md-0">
            <div id="footer-logo" class="text-white bg-black text-center"></div>
        </div>

        <div class="d-flex flex-column justify-content-between text-right">
            <div class="text-nowrap mb-2 text-body">
                &copy; compte 2020 <br>
                <a href="https://www.instagram.com/compte.construccions/" target="_blank" class="simple-hover" >instagram</a>
            </div>

            <div>
                <span class="text-nowrap text-body"> <a href="tel:+34938893380"></a>T &mdash; 93 889 33 80</span> <br>
                <a href="mailto:info@compte.tv" class="simple-hover" >info@compte.tv</a> <br>
                <a href="{{ route('privacy') }}" class="simple-hover">Legalitat</a> <br>
            </div>
        </div>

        <div id="to-top">
            <a href="#top" class="a-scroll-to"><div class="arrow"></div>&nbsp;<div class="sr-only">A dalt</div></a>
        </div>


    </div>
</footer>
