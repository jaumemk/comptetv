<div id="aside-info">

    <a href="tel:+34938893380" class="phone" data-aos="fade-right" data-aos-delay="300" data-aos-offset="0">+34938893380"</a>
    <a href="mailto:info@compte.tv" class="mail" data-aos="fade-right" data-aos-delay="400" data-aos-offset="0">info@compte.tv</a>

</div>