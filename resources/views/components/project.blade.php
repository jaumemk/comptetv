<a
    href="{{ route('project', [Str::slug($project->name, '-'), $project->id]) }}"
    class="item col {{ Str::slug($project->type, '-') }} {{ $class ?? '' }}"
     >
    <article>
        <div class="img-wrapper mb-1">
            <img src="{{ asset('uploads/projects/'.$project->thumbnail) }}" alt="" class="img-fluid">
        </div>
        <h6 class="text-secondary">{{ $project->type }}</h6>
        <h1 class="h6 font-weight-bold text-ellipsis">{{ $project->name }}</h2>
    </article>
</a>
