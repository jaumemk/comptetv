<div id="navigation-wrapper" class="fixed-top">
    <nav id="navigation" class="container-fluid px-lg-2 py-lg-1 text-center text-uppercase">
        <div id="navigation-top-bar-wrapper">
        <div id="navigation-top-bar" class="row justify-content-between align-items-center">
            <div class="col d-lg-none">
                <!-- empty flex space -->
            </div>
            <div class="col" data-aos="fade-down" data-aos-delay="250" data-aos-offset="0">
                <a id="navbar-logo" class="mr-auto ml-auto" href="{{ route('home') }}">Inici <span class="sr-only">compte.tv</span></a>
            </div>
            <div class="col d-lg-none" data-aos="fade-down" data-aos-delay="350" data-aos-offset="0">
                <button class="burger ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" onclick="this.parentNode.parentNode.parentNode.classList.toggle('active'); this.parentNode.previousElementSibling.classList.toggle('active'); this.classList.toggle('active');">
                    <span class="sr-only">{{ __('site.open-menu') }}</span>
                </button>
            </div>
        </div>
        </div>
        <div class="collapse" id="navbarNav">
            <div class="d-flex flex-column flex-lg-row px-lg-3 justify-content-between">
                <div class="d-flex justify-content-center">
                    <div class="d-none d-lg-flex" id="menu-1-toggler" data-aos="fade-down" data-aos-delay="350" data-aos-offset="0">
                        <div class="border-top d-lg-none"></div>
                        <a href="#" class="item item-action collapsed" data-toggle="collapse" data-target="#menu-1">
                            <span class="after-hover">
                                Menu
                            </span>
                        </a>
                        <a href="#" class="item item-action collapsed" data-toggle="collapse" data-target="#menu-1">
                            <span class="after-hover">
                                Tancar
                            </span>
                        </a>
                        <div class="line mx-1"><span class="border-left border-gray-light"></span></div>
                    </div>
                    <div id="menu-1" class="collapse custom">
                        <ul class="list-unstyled mb-0 d-flex flex-column flex-lg-row py-2 p-lg-0">
                            <li class="item">
                                <a href="{{ route('company') }}">
                                    <span class="after-hover">
                                        Empresa
                                    </span>
                                </a>
                            </li>
                            <li class="item">
                                <a href="{{ route('projects') }}">
                                    <span class="after-hover">
                                        Projectes
                                    </span>
                                </a>
                            </li>
                            <li class="item">
                                <a href="{{ route('contact') }}">
                                    <span class="after-hover">
                                        Contacte
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="border-top d-lg-none"></div>
                <div class="d-flex justify-content-center flex-column flex-lg-row">
                    <div class="d-flex order-lg-last pt-2 pt-lg-0" id="menu-2-toggler" data-aos="fade-down" data-aos-delay="350" data-aos-offset="0">
                        <a href="#" class="collapsed item item-action" data-toggle="collapse" data-target="#menu-2">
                            <span class="after-hover">
                                Promocions
                            </span>
                        </a>
                        <a href="#" class="collapsed item item-action" data-toggle="collapse" data-target="#menu-2">
                            <span class="after-hover">
                                Tancar
                            </span>
                        </a>
                        <div class="line mx-0 mx-lg-1 order-first"><span class="border-left border-gray-light"></span></div>
                    </div>
                    <div id="menu-2" class="collapse custom">
                        <ul class="list-unstyled mb-0 d-flex flex-column flex-lg-row pb-2 py-lg-0">
                            {{-- <li class="item">
                                <a href="{{ route('promotion', 'sert') }}">
                                    <span class="after-hover">
                                        Sert
                                    </span>
                                </a>
                            </li> --}}
                            <li class="item">
                                <a href="{{ route('promotion', 'el-nadal') }}">
                                    <span class="after-hover">
                                        El&nbsp;Nadal
                                    </span>
                                </a>
                            </li>
                            <li class="item">
                                <a href="{{ route('promotion', 'el-nadal-2') }}">
                                    <span class="after-hover">
                                        El&nbsp;Nadal 2
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="text-nowrap mb-2 d-lg-none text-body">
                            &copy; compte 2020 | <a href="https://www.instagram.com/compte.construccions/" target="_blank" class="simple-hover" >instagram</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
