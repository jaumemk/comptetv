<!doctype html>
<html class="js cssanimations" lang="ca" id="totop">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>
            {{ isset($title) ? __($title) . ' | ' : '' }} {{ 'compte.tv' }}
        </title>

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ asset('legacy/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/shared.css') }}">
        <script src="{{ asset('legacy/js/vendor/modernizr-2.8.3.min.js') }}"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129666727-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-129666727-2');
        </script>

    </head>
    <body id="@yield('wrapper-id')" class="@yield('wrapper-class') d-flex flex-column h-100 position-realtive">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <a name="top" id="top"></a>
        
        <div class="flex-shrink-0">
        @includeIf('components/header')
        <div id="page">
            @yield('page')
        </div>
        </div>

        <div class="position-relative mt-auto">
        @includeIf('components/footer')
        </div>

        @includeIf('components/aside-info')

        <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ asset('legacy/js/vendor/jquery.min.js') }}"><\/script>')</script>
        <script src="{{ asset('legacy/js/plugins.min.js') }}"></script>
        <script src="{{ asset('legacy/js/main.min.js') }}"></script>
        <script src="{{ asset('assets/js/shared.js') }}"></script>


    </body>
</html>