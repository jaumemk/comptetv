@extends('wrapper')
@section('page')

<div class="pt-4 pt-lg-5"></div>
<div class="slidedown-box text-center">
    <div class="container">
        <a href="#slidedown-2" class="title text-uppercase collapsed" data-toggle="collapse" role="button">
            Filtra els projectes
        </a>
        <div class="collapse" id="slidedown-2">
            <ul class="list-unstyled text-uppercase font-weight-bold mb-0 pt-1 pt-lg-3 pb-2">
                <li>
                    <a class="active after-hover filter-all" data-filter="*" href="#all" data-aos="fade-in" data-aos-delay="500">
                        Tots
                    </a>
                </li>
                <li>
                    <a class="after-hover filter-reformes" data-filter=".reforma" href="#reformes" data-aos="fade-in" data-aos-delay="400">
                        Reformes
                    </a>
                </li>
                <li>
                    <a class="after-hover filter-promocions" data-filter=".promocio" href="#promocions" data-aos="fade-in" data-aos-delay="350">
                        Promocions
                    </a>
                </li>
                <li>
                    <a class="after-hover filter-obra-publica" data-filter=".obra-publica" href="#obra-publica" data-aos="fade-in" data-aos-delay="300">
                        Obra pública
                    </a>
                </li>
                <li>
                    <a class="after-hover filter-particulars" data-filter=".particular" href="#particulars" data-aos="fade-in" data-aos-delay="400">
                        Particulars
                    </a>
                </li>
                <li>
                    <a class="after-hover filter-empreses" data-filter=".empresa" href="#empreses" data-aos="fade-in" data-aos-delay="500">
                        Empreses
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="pt-1 pb-4 container-fluid">
    <div id="projects-container" class="row" data-aos="fade-down" data-aos-delay="500" data-aos-offset="0">

        @foreach ($projects as $project)
            @include('components/project', ['project' => $project, 'class' => 'pb-2 col-12 col-md-6 col-lg-4 col-xl-3'])
        @endforeach


    </div>

</div>


@endsection
