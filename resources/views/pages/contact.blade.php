@extends('wrapper')
@section('page')

<section>
    <div class="mt-10 container text-center">
        <h6 class="font-weight-bold text-uppercase mb-2">Visita'ns</h6>
        <h1 class="h2 mb-2 hnt">Camprodon 13<span class="d-none d-lg-inline">,</span> <br class="d-lg-none"> entresòl 2<sup>a</sup><br>08500 Vic</h1>
    </div>
    <div class="position-relative">
        <div class="bg-black" style="position: absolute; bottom: 0; width: 100%; height: 50%; z-index: -1;">
            <!-- black block spacer to fit design requirements -->
        </div>
        <div class="container text-center">
            <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1">
                <div id="map-canvas"></div>
            </div>
            </div>
        </div>
    </div>
</section>


<section class="bg-black text-white py-4 mb-2 mb-lg-6">
    <div class="container text-center">
        <h6 class="font-weight-bold text-uppercase mb-2">Contacta'ns</h6>
        <h1 class="h2 mb-2 hnt">T'informem i assessorem <br> personalment</h1>


        <form action="{{ route('contact') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md">
                    <div class="form-group">
                        <input type="text" class="form-control border-none border-gray bg-transparent text-uppercase" name="name" id="contact-form-name" required="" placeholder="Nom i cognoms" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="col-md">
                    <div class="form-group">
                        <input type="email" class="form-control border-gray bg-transparent text-uppercase" name="email" id="contact-form-email" required="" placeholder="Email" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="col-md">
                    <div class="form-group">
                        <input type="text" class="form-control border-gray bg-transparent text-uppercase" name="phone" id="contact-form-phone" placeholder="Telèfon" value="{{ old('phone') }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <textarea class="form-control border-gray bg-transparent text-uppercase" name="comments" id="contact-form-comments" rows="1" placeholder="La teva consulta">{{ old('comments') }}</textarea>
            </div>


            <div class="row mt-2">
                <div class="col-md text-md-left">
                    <div class="form-group custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="privacy" id="contact-form-check-privacy" required="">
                        <label class="custom-control-label hnt" for="contact-form-check-privacy" style="letter-spacing: inherit;"> Accepto els <a href="{{ route('privacy') }}" class="text-light">termes i condicions</a></label>
                    </div>
                </div>
                <div class="col-md text-md-right">
                    <button type="submit" class="btn btn-white text-black text-uppercase font-brandon rounded-pill px-2">
                        <span class="font-weight-bold">
                            Enviar
                        </span>
                    </button>
                </div>
            </div>
        </form>


        @if (session('message'))
            <div class="mt-1 bg-gray font-weight-bold p-1">
                <p class="mb-0">{{ session('message') }}</p>
            </div>
        @endif


    </div>
</section>

@endsection

@push('scripts')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfQoIeLCeCFzP3j9PRcKOEgIjjQecYaSQ&libraries=&v=weekly"></script>
@endpush