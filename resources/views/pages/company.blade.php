@extends('wrapper')
@section('page')

<section>
    <div class="intro" data-aos="fade-in" data-aos-delay="0" data-aos-offset="0">
        <div class="intro-content h-100">
            <h1 class="intro-text">
                La nostra única <br> preocupació és la feina acurada, <br> polida i ben feta.
            </h1>
        </div>
    </div>
    <div class="position-relative"></div>
    <div id="services-wrapper">
        <div class="container-fluid container-md">

            <div class="slidedown-box row text-center text-md-left"  data-aos="fade-in" data-aos-delay="250" data-aos-offset="0">
                <div class="col-12 col-md-3 col-xl-2 ml-auto">
                    <a href="#slidedown-1" class="title d-block text-uppercase collapsed" data-toggle="collapse" role="button">
                        Serveis
                    </a>
                </div>
                <div class="w-100 d-none d-sm-block"></div>
                <div class="col-12 col-md-3 col-xl-2 ml-auto">
                    <div class="collapse" id="slidedown-1">
                        <ul class="list-unstyled text-uppercase font-weight-bold mb-0 py-1 py-md-0">
                            <li>
                                <a href="#reformes" class="a-scroll-to">
                                    Reformes
                                </a>
                            </li>
                            <li>
                                <a href="#promocions" class="a-scroll-to">
                                    Promocions
                                </a>
                            </li>
                            <li>
                                <a href="#obra-publica" class="a-scroll-to">
                                    Obra pública
                                </a>
                            </li>
                            <li>
                                <a href="#particulars" class="a-scroll-to">
                                    Particulars
                                </a>
                            </li>
                            <li>
                                <a href="#empreses" class="a-scroll-to">
                                    Empreses
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="desc">
        <div class="container">
            <div class="row py-1 py-md-0">
                <div class="d-none d-md-block col-4 position-relative">

                    <div class="title-in-circle" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                        <div class="position-relative text-center h-100">
                            <div class="title-in-circle-inner">
                                <h2 class="text-uppercase h6 font-weight-bold mb-0">Construïm</h2>
                                <h3 class="m-0 letter-spacing-small hnt">Qualitat de vida</h3>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 py-md-4 text-uppercase" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">
                    <p>
                        Som constructors i ens apassiona la nostra feina. En un moment en què l'únic que importa és la quantitat envers la qualitat a compte fugim d'aquesta manera de fer i ens centrem en el què ens fa realment viure: construir llars de qualitat.
                    </p>
                    <p>
                        Les nostres cases, els nostres pisos, no són mercaderies, per a nosaltres, són el resultat final d'un llarg i curós procés que finalitza el dia que
                    </p>
                </div>
                <div class="col-md-4 py-md-4 text-uppercase" data-aos="fade-in" data-aos-delay="350" data-aos-offset="0">
                    <p>
                        s'hi entra a viure, perquè aquest és l'últim i més honrat objectiu de les nostres construccions. Som plenament conscients de com d'important és, avui en dia, decidir-se a donar aquest pas i tant sols per aquest motiu, posem per davant de qualsevol altre consideració, la qualitat.
                    </p>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="bg-black pt-4 pb-4 text-white">

    <h1 class="h2 text-center mb-4 hnt font-weight-light value-title" data-aos="fade-in" data-aos-delay="300" data-aos-offset="0">Els nostres valors</h1>

    <div class="container">
        <ul id="company-values" class="list-unstyled row font-weight-bold large h6 text-uppercase mb-0">
            <li data-aos="fade-in" data-aos-delay="300" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-social.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Responsabilitat <br> social</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="350" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-experiencia.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Professionalitat <br> i experiència</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="400" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-innovacio.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Innovació <br> tecnològica</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="450" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-medi.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Respecte amb <br> el medi ambient</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="300" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-sostenibilitat.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Sostenibilitat <br> i eficiència energètica</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="350" data-aos-offset="0" class="col-md-4 mb-2 d-flex align-items-center">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-huma.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Capital humà</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="400" data-aos-offset="0" class="col-md-4 mb-2 d-flex align-items-center">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-qualitat.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Qualitat i garantia</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="450" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-compromis.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Compromís amb els <br> nostres treballadors</div>
            </li>
            <li data-aos="fade-in" data-aos-delay="500" data-aos-offset="0" class="col-md-4 mb-2 d-flex">
                <div class="mr-1">
                    <img src="{{ asset('images/icons/valor-proveidors.svg') }}" alt="" height="48" width="48">
                </div>
                <div>Proveïdors <br> de confiança</div>
            </li>
        </ul>
    </div>

</section>

<div class="pt-2">

    <section id="reformes" data-aos="fade-in" data-aos-delay="200" data-aos-offset="0">
        <div class="container">
            <div class="row pt-2 pb-1 pb-md-2">
                <div class="col">
                    <h1 class="h2  hnt">Reformes</h1>
                </div>
                <div class="col text-right text-uppercase d-none d-md-block">
                    <a href="{{ route('projects') }}#reformes" class="btn btn-dark rounded-pill" role="button">
                        <span class="font-weight-bold">
                            Veure projectes
                        </span>
                    </a>
                </div>
            </div>

            <div class="row text-uppercase">
                <div class="col-md-5 col-lg-4">
                    <div class="text-center button-overlayed">
                        <img src="{{ asset('images/1-reformes.jpg') }}" alt="" class="img-fluid mb-md-1">
                        <a href="{{ route('projects') }}#reforme{{ route('projects') }}#reformes" class="btn btn-dark rounded-pill d-md-none" role="button">
                            <span class="font-weight-bold">
                                Veure projectes
                            </span>
                        </a>
                    </div>
                </div>
                <div class="offset-lg-1 col-md">
                    <div class="row">
                        <div class="col-lg">
                            <h6 class="line-height-larger font-weight-bold mb-1">
                                Reformes complertes de pisos, cuines, banys, terres
                            </h6>
                            <div class="">
                                <p>
                                    Dedicats a la reforma integral, especialitzats en banys, cuines i terres. Des de petites reformes a habitatges sencers, amb més de 25 anys d'experiència que ens avalen.
                                </p>
                            </div>

                            <h6 class="line-height-larger font-weight-bold mt-1 mb-1">
                                Adequació de façanes, sustitució de tancaments i millora de l’aïllament tèrmic
                            </h6>
                            <div class="">
                                <p>
                                    Disminuir les pèrdues de calor reforçant els aïllaments tèrmics o col·locant-ne de nous pot reduir el consum d’energia, tant de la calefacció com de la refrigeració, entre el 20 i el 40%. adaptem la façana i l’aïllem convenientment depenent de cada cas, amb aïllaments per l’exterior, aïllaments per la càmera d’aire, o aïllaments interiors.
                                </p>
                            </div>
                        </div>
                        <div class="col-lg">
                            <h6 class="line-height-larger font-weight-bold mb-1">
                                Millora per l’accessibilitat de la gent gran amb ascensors
                            </h6>
                            <div class="">
                                <p>
                                    La col·locació dels ascensors i/o plataformes salvaescales suposa una millora en l’accessibilitat per a la majoria dels habitatges. ens cuidem de tots els tràmits, projecte, llicència i tràmits de subvenció.
                                </p>
                            </div>
                            <h6 class="line-height-larger font-weight-bold mt-1 mb-1">
                                Adequació d’instal·lacions per la millora de l’estalvi energètic
                            </h6>
                            <div class="">
                                <p>
                                    el nostre objectiu és que pugueu gaudir d’una temperatura agradable a un preu avantatjós, amb unes instal·lacions modernes, amb uns rendiments alts i uns consums moderats, amb energies sostenibles. estudiem cada cas i proposem la millor alternativa com la geotèrmia, biomassa, aerotèrmia, entre altres, tenint en compte consums, potències i amortització del cost.
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <hr>

    <section id="promocions" data-aos="fade-in" data-aos-delay="200" data-aos-offset="0">
        <div class="container">
            <div class="row pt-2 pb-1 pb-md-2">
                <div class="col">
                    <h1 class="h2  hnt">Promocions</h1>
                </div>
                <div class="col text-right text-uppercase d-none d-md-block">
                    <a href="{{ route('projects') }}#promocions" class="btn btn-dark rounded-pill" role="button">
                        <span class="font-weight-bold">
                            Veure projectes
                        </span>
                    </a>
                </div>
            </div>
            <div class="row text-uppercase">
                <div class="col-md-5 col-lg-4">
                    <div class="text-center button-overlayed">
                        <img src="{{ asset('images/2-promocions.jpg') }}" alt="" class="img-fluid mb-md-1">
                        <a href="{{ route('projects') }}#promocions" class="btn btn-dark rounded-pill d-md-none" role="button">
                            <span class="font-weight-bold">
                                Veure projectes
                            </span>
                        </a>
                    </div>
                </div>
                <div class="offset-lg-1 col-md">
                    <div class="">
                        <p>
                            Totes les promocions que realitzem estan a les nostres mans, des del principi fins al final. les pensem i les decidim. les construïm i les comercialitzem. col·laborem amb destacats professionals de l’arquitectura i la construcció que elaboren i coordinen els projectes de principi a final.
                        </p>
                        <p>
                            Treballem amb bons industrials i proveïdors de confiança per poder garantir els nostres compromisos i la màxima qualitat. tenim un servei post-venda propi, que ens permet resoldre els problemes dels nostres clients fins i tot un cop acabada l’obra.
                        </p>
                    </div>
                </div>
            </div>
    </section>

    <hr>

    <section id="obra-publica" data-aos="fade-in" data-aos-delay="200" data-aos-offset="0">
        <div class="container">
            <div class="row pt-2 pb-1 pb-md-2">
                <div class="col">
                    <h1 class="h2  hnt">Obra pública</h1>
                </div>
                <div class="col text-right text-uppercase d-none d-md-block">
                    <a href="{{ route('projects') }}#obra-publica" class="btn btn-dark rounded-pill" role="button">
                        <span class="font-weight-bold">
                            Veure projectes
                        </span>
                    </a>
                </div>
            </div>
            <div class="row text-uppercase">
                <div class="col-md-5 col-lg-4">
                    <div class="text-center button-overlayed">
                        <img src="{{ asset('images/3-obra-publica.jpg') }}" alt="" class="img-fluid mb-md-1">
                        <a href="{{ route('projects') }}#obra-publica" class="btn btn-dark rounded-pill d-md-none" role="button">
                            <span class="font-weight-bold">
                                Veure projectes
                            </span>
                        </a>
                    </div>
                </div>
                <div class="offset-lg-1 col-md">
                    <div class="">
                        <p>
                            La total dedicació i la gran experiència del nostre equip tècnic ens permet assumir amb total confiança i responsabilitat l’execució de projectes de l’administració pública, amb la seguretat del compliment dels terminis d’execució.
                        </p>
                        <p>
                            Els requisits tècnics i pressupostaris de l’obra contractada són claus per definir el resultat final del projecte. les obres públiques realitzades fins al moment estan formades per una àmplia varietat de projectes.
                        </p>
                    </div>
                </div>
            </div>
    </section>

    <hr>

    <section id="particulars" data-aos="fade-in" data-aos-delay="200" data-aos-offset="0">
        <div class="container">
            <div class="row pt-2 pb-1 pb-md-2">
                <div class="col">
                    <h1 class="h2  hnt">Particulars</h1>
                </div>
                <div class="col text-right text-uppercase d-none d-md-block">
                    <a href="{{ route('projects') }}#particulars" class="btn btn-dark rounded-pill" role="button">
                        <span class="font-weight-bold">
                            Veure projectes
                        </span>
                    </a>
                </div>
            </div>
            <div class="row text-uppercase">
                <div class="col-md-5 col-lg-4">
                    <div class="text-center button-overlayed">
                        <img src="{{ asset('images/4-particulars.jpg') }}" alt="" class="img-fluid mb-md-1">
                        <a href="{{ route('projects') }}#particulars" class="btn btn-dark rounded-pill d-md-none" role="button">
                            <span class="font-weight-bold">
                                Veure projectes
                            </span>
                        </a>
                    </div>
                </div>
                <div class="offset-lg-1 col-md">
                    <div class="">
                        <p>
                            La construcció, a dia d’avui, és una de les formes més importants d’augmentar la qualitat de vida. tenir una llar ben construïda amb tots els serveis necessaris és una prioritat per nosaltres i, per aquest motiu, t’oferim l’assessorament en els teus projectes i tràmits totalment personalitzada.
                        </p>
                        <p>
                            Orientem tots els nostres recursos i esforços a la identificació i satisfacció sistemàtica de les necessitats dels nostres clients. cercant la millora de la qualitat de les nostres obres i activitats, assegurem la rendibilitat i competitivitat, procurant la millora de l'eficiència dels nostres processos.
                        </p>
                    </div>
                </div>
            </div>
    </section>

    <hr>

    <section id="empreses" data-aos="fade-in" data-aos-delay="200" data-aos-offset="0">
        <div class="container">
            <div class="row pt-2 pb-1 pb-md-2">
                <div class="col">
                    <h1 class="h2  hnt">Empreses</h1>
                </div>
                <div class="col text-right text-uppercase d-none d-md-block">
                    <a href="{{ route('projects') }}#empreses" class="btn btn-dark rounded-pill" role="button">
                        <span class="font-weight-bold">
                            Veure projectes
                        </span>
                    </a>
                </div>
            </div>
            <div class="row text-uppercase">
                <div class="col-md-5 col-lg-4">
                    <div class="text-center button-overlayed">
                        <img src="{{ asset('images/5-empreses.jpg') }}" alt="" class="img-fluid mb-md-1">
                        <a href="{{ route('projects') }}#empreses" class="btn btn-dark rounded-pill d-md-none" role="button">
                            <span class="font-weight-bold">
                                Veure projectes
                            </span>
                        </a>
                    </div>
                </div>
                <div class="offset-lg-1 col-md">
                    <div class="">
                        <p>
                            Des dels orígens, un dels nostres objectius ha estat la construcció personalitzada d’edificacions siginificatives i de gran qualitat, com a substancial valor afegit. per a empreses, sempre hem intentat treballar amb productes d'última generació, que ens permeten obtenir projectes singulars.
                        </p>
                        <p>
                            Estem presents tant en el sector públic com al privat, i gràcies al nostre sistema de gestió i assessorament, juntament amb l’experiència i la capacitat adquirida amb els anys, estem a disposició de satisfer totes les necessitats dels nostres clients, garantint els resultats.
                        </p>
                    </div>
                </div>
            </div>
    </section>

</div>
@endsection
