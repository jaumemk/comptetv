@extends('wrapper')
@section('page')

<div class="pt-6 pb-4">

    <div class="container">
        <div class="text-center">
            <h6 class="font-weight-bold text-uppercase mb-05" data-aos="fade-down" data-aos-delay="200" data-aos-offset="0">{{ $project->type }}</h6>
            <h1 class="h2 mb-2 hnt project-title" data-aos="fade-up" data-aos-delay="300" data-aos-offset="0"> {{ $project->name }}</h1>
        </div>
    </div>

    <div id="project-slider" data-aos="fade-in" data-aos-delay="400" data-aos-offset="0">
        @foreach ($project->gallery as $image)
        @if($image != '---')
        <div class="inner-slide">
            <div class="container no-gutters">
                <div class="col-md-9 mx-auto text-center">
                    <img src="{{ asset('uploads/projects/'.$image) }}" alt="" class="img-fluid mx-auto">
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>

    <div class="bg-gray-lighter pt-14 pb-8 nmt-10" data-aos="down" data-aos-delay="300" data-aos-offset="0">
        <div class="container project-description">
            <div class="col-md-8 mx-auto">
                <div class="row">
                    <div class="col-md-8">
                        <h2 class="h6 text-uppercase text-secondary">Descripció</h2>
                        <p class="text-uppercase">
                            {{ $project->description }}
                        </p>
                    </div>
                    <div class="col-md-1 text-center">
                        <div class="d-none h-100 d-md-inline-block border-right border-gray-light"></div>
                        <div class="d-md-none border-top border-gray-light pb-2 mt-1"></div>
                    </div>
                    <div class="col-md-2 text-uppercase">
                        <div class="row">
                            <div class="col-6 col-md-12 mb-1">
                                <h2 class="h6 text-uppercase text-secondary">Població</h2>
                                {{ $project->city }}
                            </div>
                            <div class="col-6 col-md-12 mb-1">
                                <h2 class="h6 text-uppercase text-secondary">Any</h2>
                                {{ $project->year }}
                            </div>
                            <div class="col-6 col-md-12">
                                <h2 class="h6 text-uppercase text-secondary">Superfície</h2>
                                {{ $project->area }}
                                {{-- M<sup>2</sup> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container pt-4 text-uppercase">
        <div class="row justify-contents-between">
            <div class="col-5 col-lg-4 col-xl-3 text-right text-md-left">
                <a href="{{ route('project', [Str::slug($prevProject->name, '-'), $prevProject->id]) }}" class="btn btn-dark rounded-pill px-1 px-md-2 btn-block" role="button">
                    <span class="font-weight-bold">
                        Anterior <span class="d-none d-lg-inline">projecte</span>
                    </span>
                </a>
            </div>
            <div class="col text-center">
                <a href="{{ route('projects') }}" class="btn btn-link d-none d-lg-block"><span class="after-hover">Tots els projectes</span></a>
            </div>
            <div class="col-5 col-lg-4 col-xl-3 text-md-right text-left">
                <a href="{{ route('project', [Str::slug($nextProject->name, '-'), $nextProject->id]) }}" class="btn btn-dark rounded-pill px-1 px-md-2 btn-block" role="button">
                    <span class="font-weight-bold">
                        Següent <span class="d-none d-lg-inline">projecte</span>
                    </span>
                </a>
            </div>
        </div>

        <div class="text-center d-lg-none mt-2">
            <a href="{{ route('projects') }}" class="btn btn-link"><span class="after-hover">Tots els projectes</span></a>
        </div>
    </div>


</div>

@endsection
