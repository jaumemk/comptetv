@extends('wrapper')
@push('styles')
<style>
    #norwegian {
        background: linear-gradient(225deg, #ffc27d, #ff8e93);
    }

    #french {
        background: linear-gradient(#f3defc, #63a3e6);
    }

    #spanish {
        background: linear-gradient(#d2fcad, #22eaeb);
    }

    #hindi {
        background: linear-gradient(#f0bed4, #a163f5);
    }

    #mandarin {
        background: linear-gradient(#65cce1, #6365ec);
    }

    section>span {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        font-size: 7vw;
        font-family: sans-serif;
        text-transform: uppercase;
        color: #fff;
    }

</style>
@endpush
@section('page')
<div class="slides">

    <section id="el-nadal" class="text-white overflow-hidden">

        <div class="d-flex flex-column wide-content">
            <div class="flex-shrink-1">
                <div class="pt-8 pt-4 flex-shrink-1"></div>
            </div>
            <div class="flex-grow-1 position-relative position-relative px-lg-4">

                <div class="d-flex flex-column flex-lg-row justify-content-between align-items-baseline w-100 text-center pb-3">
                    <h2 class="mb-0 text-lg-left w-100" data-aos="fade-down" data-aos-delay="300">Promoció 1</h2>
                    <h1 class="mb-0 display-1 text-white bs font-weight-bold w-100" data-aos="fade-down" data-aos-delay="400">El&nbsp;Nadal</h1>
                    <div class="d-none d-lg-block w-100 text-right" data-aos="fade-down" data-aos-delay="350">
                        <a href="{{ route('promotion', 'el-nadal') }}" class="btn btn-dark rounded-pill px-1 px-md-2 text-uppercase" role="button">
                            <span class="font-weight-bold">
                                Veure promoció
                            </span>
                        </a>
                    </div>
                </div>

                <div class="position-absolute d-none d-lg-block" data-aos="fade-in" data-aos-delay="400">
                    <div class="row no-gutters">
                        <div class="col-6 col-lg-5 col-xl-3">
                            <p class="text-uppercase">
                                A compte t’oferim una nova promoció al vostre abast, a la zona del nadal de Vic.
                                Amb pàrquing, sense locals comercials i pisos construïts per Compte,
                                amb la garantia i qualitat que això significa. En parlem?
                            </p>
                        </div>
                    </div>
                </div>

                <div class="vertical-block position-relative">
                    <div class="ml-auto w-75 w-lg-50 text-right text-lg-left position-relative pr-1 pr-lg-0 sreq-img-1" style="z-index: 1;">
                        <img src="{{ asset('images/el-nadal.png') }}" alt="" class="img-fluid" style="" data-aos="fade-left" data-aos-delay="500">
                        <div class="title-in-circle-2 position-absolute d-none d-lg-flex" style="top: 100%; right: 0; margin-top: -15%;" data-aos="fade-in" data-aos-delay="300">
                            <h3 class="display-4 m-0 letter-spacing-small bs font-weight-bold text-center">Últims<br>pisos!</h3>
                        </div>
                    </div>
                    <div class="w-75 w-lg-50 sreq-img-2" style="z-index: 0;">
                        <img src="{{ asset('images/el-nadal.jpg') }}" alt="" class="img-fluid" data-aos="fade-right" data-aos-delay="500">
                    </div>
                </div>
            </div>

            <div class="text-center py-4 d-lg-none" data-aos="fade-in" data-aos-delay="500">
                <a href="{{ route('promotion', 'el-nadal') }}" class="btn btn-dark rounded-pill px-1 px-md-2 text-uppercase" role="button">
                    <span class="font-weight-bold">
                        Veure promoció
                    </span>
                </a>
            </div>

            <div class="next-section">
                <a href="#sert" class="a-scroll-to">
                    <div class="arrow"></div>
                </a>
            </div>

        </div>

    </section>

    <section class="bg-white overflow-hidden position-relative" id="sert">

        <div class="d-flex flex-column wide-content">
            <div class="flex-shrink-1">
                <div class="pt-4 flex-shrink-1"></div>
            </div>
            <div class="flex-grow-1 position-relative position-relative px-lg-4">

                <div id="logo-sert" class="position-absolute w-100" style="top: 0; left: 0; z-index: 0;">
                    <div class="row no-gutters justify-content-center">
                        <div class="col-7 col-sm-5 col-md-4 col-lg-3 col-xl-2" data-aos="fade-in" data-aos-delay="500">
                            <img src="{{ asset('images/sert.svg') }}" alt="" class="img-fluid d-none d-md-block">
                            <img src="{{ asset('images/sert-gray.svg') }}" alt="" class="img-fluid d-block d-md-none">
                        </div>
                    </div>
                </div>

                <div class="d-flex flex-column flex-lg-row justify-content-between align-items-baseline w-100 text-center pb-3 pt-2 mb-5">
                    <h2 class="mb-0 text-lg-left w-100" data-aos="fade-down" data-aos-delay="300">Promoció 2</h2>
                    <div class="d-none d-lg-block w-100 text-right" data-aos="fade-down" data-aos-delay="350">
                        <a href="{{ route('promotion', 'sert') }}" class="btn btn-dark rounded-pill px-1 px-md-2 text-uppercase" role="button">
                            <span class="font-weight-bold">
                                Veure promoció
                            </span>
                        </a>
                    </div>
                </div>

                <div class="position-absolute d-none d-lg-block" data-aos="fade-in" data-aos-delay="400">
                    <div class="row no-gutters">
                        <div class="col-6 col-lg-5 col-xl-3">
                            <p class="text-uppercase">
                                A cinc minuts caminant del centre de vic, emprant les millors marques i construït per compte, us hi espera la vostra nova llar. una oportunitat única de viure en una tranquil·la zona enjardinada i sense locals comercials, per assegurar-ne la màxima qualitat de vida.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="vertical-block position-relative" style="z-index: 1;">
                    <div class="ml-auto w-75 w-lg-50 text-right text-lg-left position-relative pr-1 pr-lg-0 sreq-img-1" style="z-index: 1;">
                        <img src="{{ asset('images/sert.png') }}" alt="" class="img-fluid" style="" data-aos="fade-left" data-aos-delay="500">
                        <div class="title-in-circle-3 position-absolute d-none d-lg-flex" style="top: 100%; right: 0; margin-top: -15%;" data-aos="fade-in" data-aos-delay="300">
                            <h3 class="display-4 m-0 letter-spacing-small bs font-weight-bold text-center">Últim<br>pis!</h3>
                        </div>
                    </div>
                    <div class="w-75 w-lg-50 sreq-img-2" style="z-index: 0;">
                        <img src="{{ asset('images/sert.jpg') }}" alt="" class="img-fluid" data-aos="fade-right" data-aos-delay="500">
                    </div>
                </div>
            </div>

            <div class="text-center py-4 d-lg-none" data-aos="fade-in" data-aos-delay="500">
                <a href="{{ route('promotion', 'sert') }}" class="btn btn-dark rounded-pill px-1 px-md-2 text-uppercase" role="button">
                    <span class="font-weight-bold">
                        Veure promoció
                    </span>
                </a>
            </div>

            <div class="next-section">
                <a href="#about" class="a-scroll-to">
                    <div class="arrow"></div>
                </a>
            </div>

        </div>

    </section>

    <section class="bg-gray-lighter min-vh-100 position-relative" id="about">

        <div class="d-flex flex-column wide-content">
            <div class="container-fluid px-lg-3">
                <div class="row pt-4 pt-lg-12 pb-4">
                    <div class="col-lg-7 offset-lg-2">
                        <h1 class="display-lg-1 bs " data-aos="fade-down" data-aos-delay="500">
                            C&nbsp;de&nbsp;Compte<br>
                            C&nbsp;de&nbsp;Construccions
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="wide-content container-fluid">
            <div class="row no-gutters">
                <div class="col-12 col-lg-8 bg-pattern pb-8">

                    <div class="row">
                        <div class="col-10 col-lg-12" data-aos="fade-right" data-aos-delay="200">
                            <img src="{{ asset('images/casc.png') }}" alt="" class="img-fluid">
                        </div>
                    </div>

                    <div class="d-lg-none text-center text-lg-left pt-4">
                        <a href="{{ route('company') }}" class="btn btn-dark rounded-pill px-2 text-uppercase" role="button">
                            <span class="font-weight-bold">
                                Coneix-nos
                            </span>
                        </a>
                    </div>

                </div>
                <div class="col d-none d-lg-block">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-7 offset-lg-1 col-xl-6 offset-xl-2 ">
                            <p class="text-uppercase" data-aos="fade-down" data-aos-delay="300">
                                La nostra única preocupació és la feina acurada, polida i ben feta.
                            </p>
                            <div class="text-center text-lg-left pt-2 pb-4" data-aos="fade-up" data-aos-delay="200">
                                <a href="{{ route('company') }}" class="btn btn-dark rounded-pill px-2 text-uppercase" role="button">
                                    <span class="font-weight-bold">
                                        Coneix-nos
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="next-section">
            <a href="#last-projects" class="a-scroll-to">
                <div class="arrow"></div>
            </a>
        </div>

    </section>
    <div>

        <section class="pb-6 pt-3 container-fluid px-md-3" id="last-projects">


            <div class="row no-gutters px-0 pb-2 px-lg-3 pb-lg-4">
                <div class="col-12 col-lg-4 offset-lg-4">
                    <h1 class="ml-auto h2 mb-0 text-center" data-aos="fade-in" data-aos-delay="200">Últims projectes</h1>
                </div>
                <div class="col-4 d-none d-lg-block text-right" data-aos="fade-in" data-aos-delay="300">
                    <a href="{{ route('projects') }}" class="btn btn-dark rounded-pill px-1 px-md-2 text-uppercase" role="button">
                        <span class="font-weight-bold">
                            Més projectes
                        </span>
                    </a>
                </div>
            </div>

            <div id="last-projects-slider" class="row mb-6">
                @foreach ($projects as $project)
                <div data-aos="fade-down" data-aos-delay="{{ $loop->index * 100 }}" data-aos-offset="0">
                    @include('components/project', $project)
                </div>
                @endforeach
            </div>

            <div class="d-block d-lg-none w-100 text-center" data-aos="fade-in" data-aos-delay="400">
                <a href="{{ route('projects') }}" class="btn btn-dark rounded-pill px-1 px-md-2 text-uppercase" role="button">
                    <span class="font-weight-bold">
                        Més projectes
                    </span>
                </a>
            </div>

        </section>
        <div class="position-relative">
            @includeIf('components/footer')
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script src="{{ mix('assets/js/vslider.js') }}" defer></script>
@endpush