@extends('legacy')
@section('wrapper-class', 'legacy')
@section('wrapper-id', 'home')
@section('page')

    <section class="highlight animated fadeIn slow">
        <div class="container promo-s">
              <div class="row h-100">
                <div class="col-2 title-zone h-100">
                    <h2>Promoció residencial Sert</h2>
                    <p>C. Josep Maria Sert 08500 Vic</p>
                    <p id="counter"><span id="frac">1</span>/<span id="total">10</span></p>
                </div>
                <div class="col-2 title-zone h-100">
                </div>
                 <div id="carousel-splash">
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-01.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-02.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-03.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-04.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-05.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-06.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-07.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-08.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-09.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-10 col-xl-12 pdl">
                                        <img class="img-fluid animated main" src="{{ asset('/legacy/img/slide-10.jpg') }}" alt="Slide">
                                    </div>
                                    <div class="col-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

    <section id="promo-sert" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
        <div class="container">
            <div class="row">
                    <div class="col-md-4 offset-1">
                        <h2>Promoció Sert, a 5 minuts <br>de la Plaça Major</h2>
                        <p>La promoció Josep Maria Sert és un conjunt residencial de pisos d’obra nova situat a 5 minuts de la Plaça Major de Vic, en un entorn tranquil i privilegiat envoltat de zones verdes i sense locals comercials.</p>
                        <p>És una promoció dissenyada pensant en el confort, la qualitat i la sostenibilitat i que ofereix un ampli ventall d’habitatges de fins a 212m² amb terrasses, jardins i places de pàrquing per adaptar-se a les necessitats i estils de vida dels futurs propietaris.</p>
                        <div class="row">
                            <div class="container specs">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-lg-5">
                                        <h4>Població</h4>
                                        <p>Vic, Barcelona</p>
                                        <h4>Dormitoris</h4>
                                        <p>3 i 4 habitacions</p>
                                        <h4>Terrassa / Jardí</h4>
                                        <p>Fins a 150 m²</p>
                                    </div>
                                    <div class="col-12 col-sm-6 col-lg-7">
                                        <h4>Any</h4>
                                        <p>2018</p>
                                        <h4>Sup. construïda</h4>
                                        <p>De 110 m² a 212 m²</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 offset-md-1 sert-img">
                        <div id="resert">
                            <img src="{{ asset('/legacy/svg/sert.svg') }}" class="img-fluid" alt="Sert">
                        </div>
                        <img src="{{ asset('/legacy/img/promocio-sert-compte.jpg') }}" class="img-fluid" alt="Promoció Sert">
                    </div>
            </div>
        </div>
        <img src="{{ asset('/legacy/img/bg-m.jpg') }}" class="bg-m" alt="bg-m">
    </section>

    <section id="why" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>

        <div class="container">
            <div class="row row-eq-height">

                <div class="col-md-12 offset-3 offset-xs-2 offset-md-0">
                        <h2>Perquè escollir Sert?</h2>
                </div>
                <div class="inner col-md-5 col-lg-3 offset-3 offset-xs-2 offset-md-1">
                    <h3 id="loc">localització</h3>
                    <p>A 5 minuts de la Plaça de Vic situat en un entorn tranquil i envoltat de zones verdes i tots els serveis necessaris.</p>
                </div>
                <div class="inner col-md-5 col-lg-3 offset-3 offset-xs-2 offset-md-1">
                    <h3 id="energy">Qualificació energètica</h3>
                    <p>L’edifici té la qualificació d’eficiència energètica A, el que suposa una disminució de les emissions de CO₂ i una reducció significativa de la demanda energètica de l’edifici gràcies, entre altres coses, a l’ús de l’aerotermia per a la climatització dels habitatges.</p>
                </div>
                <div class="inner col-md-5 col-lg-3 offset-3 offset-xs-2 offset-md-1">
                    <h3 id="interiorism">Projecte d'interiorisme</h3>
                    <p>Oferim als nostres clients el servei de realitzar un projecte d’interiorisme gratuït de la seva futura vivenda adequant l’espai a les seves necessitats i gustos pensant no només en la estètica, sinó també en la funcionalitat.</p>
                </div>
                <div class="inner col-md-5 col-lg-3 offset-3 offset-xs-2 offset-md-1">
                    <h3 id="parking">Pàrquings</h3>
                    <p>L’edifici disposa de garatge amb accés de vehicles a través d’una porta automàtica accionada amb comandament a distancia. El pàrquing es comunica amb les vivendes a través de l’escala principal i l’ascensor que es troben en els vestíbuls.</p>

                </div>
                <div class="inner col-md-5 col-lg-3 offset-3 offset-xs-2 offset-md-1">
                    <h3 id="exterior">Exteriors</h3>
                    <p>Es tracta d’un edifici de disseny, amb una façana d’estètica moderna i identitat marcada construïda amb materials molt resistents, perdurables en el temps i que suposen molt poc manteniment.</p>
                </div>
                <div class="inner col-md-5 col-lg-3 offset-3 offset-xs-2 offset-md-1">
                    <h3 id="quality">Primeres qualitats</h3>
                    <p>Tot l’edifici està equipat amb una gama de productes i materials basats en els màxims estàndards de qualitats que ens ofereixen les millors marques del sector.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="tipology" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 offset-md-1 xlargehalf">
                    <h2>Tipologies</h2>
                </div>
                <div class="col-12 col-md-6 lightbox-inner zoomin">
                    <a id ="zoom" class="btn btn-primary zoom" href="img/tipologies/tipologies-0.png" role="button">Ampliar <div class="img search"></div></a>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div id="tip-slide" class="lightbox-inner">
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-0.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-0.png') }}" alt="Planta baixa 2">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-1.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-1.png') }}" alt="Planta baixa 3">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-2.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-2.png') }}" alt="Planta baixa 4">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-3.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-3.png') }}" alt="Pis primer segona">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-4.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-4.png') }}" alt="Pis primer tercera">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-5.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-5.png') }}" alt="Pis segon primera">
                            </a>
                        </div>

                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-6.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-6.png') }}" alt="Pis segon segona">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-7.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-7.png') }}" alt="Pis segon tercera">
                            </a>
                        </div>


                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-8.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-8.png') }}" alt="Pis tercer primera">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-9.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-9.png') }}" alt="Pis tercer segona">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-10.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-10.png') }}" alt="Pis quart primera">
                            </a>
                        </div>
                        <div class="item">
                             <a class="zoom-inner" href="img/tipologies/tipologies-11.png">
                                <img class="slidew img-fluid" src="{{ asset('/legacy/img/tipologies/tipologies-11.png') }}" alt="Àtic">
                            </a>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

    <section id="tipinfo" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
        <div class="container">
            <div class="row align-items-end d-flex align-items-stretch">
                <div class="col-md-11 offset-md-1 col-lg-9 offset-lg-1 grey xlarge flats">
                    <h2 class="title">Pis de <br>4 habitacions</h2>
                    <div class="row">
                        <div class="container specs">
                            <div class="row grey-inner">
                                <div class="col-md-5">
                                    <p class="feat lead">Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.</p>
                                </div>
                                <div class="col offset-md-1">
                                    <div class="row">
                                        <div class="container specs">
                                            <div class="row">
                                                <div class="col-6">
                                                    <h4>Dormitoris</h4>
                                                    <p class="rooms">4 habitacions</p>
                                                    <h4>Banys</h4>
                                                    <p class="bath">2</p>
                                                </div>
                                                <div class="col-6">
                                                    <h4>Sup. Construïda</h4>
                                                    <p class="surface">112 m²</p>
                                                    <h4>Terrassa / Jardí</h4>
                                                    <p class="terrace">18 m²</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-12 col-lg-2 black black-zone d-lg-flex align-content-end flex-wrap">
                    <div class="row">
                    <div class="container d-lg-flex">
                        <div class="row align-content-end">
                            <div class="col-12 col-sm-6 col-sm-offset-0 col-md-5 offset-md-1 col-lg-12 offset-lg-0">

                                    <h4>Descàrregues</h4>
                                    <a href="#" id="download" class="btn btn-secondary zoom second" download>Plànols <div class="img download"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 15"><path d="M12 14c0-.6-.4-1-1-1H1c-.6 0-1 .4-1 1s.4 1 1 1h10c.6 0 1-.4 1-1m-1.3-7.7c-.4-.4-1-.4-1.4 0L7 8.6V1c0-.6-.4-1-1-1S5 .4 5 1v7.6L2.7 6.3c-.4-.4-1-.4-1.4 0-.4.4-.4 1 0 1.4l4 4c.2.2.4.3.7.3.3 0 .5-.1.7-.3l4-4c.4-.4.4-1 0-1.4"/></svg></div></a>

                            </div>
                            <div class="col-12 col-sm-6 col-lg-12">
                                    <h4>Compartir</h4>
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A//twitter.com/home?status=http%253A//promocions.compte.tv/" class="facebook" target="blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><path d="M16.6 24h-3.7v-9H11v-3.1h1.9V10c0-2.5 1.1-4 4-4h2.5v3.1h-1.6c-1.2 0-1.2.4-1.2 1.2v1.6h2.8l-.3 3.1h-2.5v9z"/></svg></a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/home?status=http%3A//promocions.compte.tv/" class="twitter" target="blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><path d="M23.4 9.8c-.6.3-1.3.5-2 .5.7-.4 1.3-1.1 1.5-1.9-.7.4-1.4.7-2.2.8-.6-.7-1.5-1.1-2.5-1.1-1.9 0-3.5 1.5-3.5 3.5 0 .3 0 .5.1.8-2.9-.1-5.4-1.5-7.1-3.6-.3.5-.5 1.1-.5 1.7 0 1.2.6 2.3 1.5 2.9-.6 0-1.1-.2-1.6-.4 0 1.7 1.2 3.1 2.8 3.4-.3.1-.6.1-.9.1-.2 0-.4 0-.7-.1.4 1.4 1.7 2.4 3.2 2.4-1.2.9-2.7 1.5-4.3 1.5h-.8c1.5 1 3.4 1.6 5.3 1.6 6.4 0 9.8-5.3 9.8-9.9v-.4c.9-.5 1.5-1.1 1.9-1.8"/></svg></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </section>


    <section id="aero" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
        <div class="container">
            <div class="row">

                    <svg version="1.1" id="alogomb" class="mb" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 90 90" xml:space="preserve"><style>.st0{fill:#fff}</style><path d="M45 90c24.9 0 45-20.1 45-45S69.9 0 45 0 0 20.1 0 45s20.1 45 45 45"/><path class="st0" d="M44.4 47.6c-1.3-.4-2.1-1.7-1.7-3 .4-1.3 1.7-2.1 3-1.7 1.3.3 2.1 1.7 1.7 3-.3 1.1-1.3 1.8-2.4 1.8-.2 0-.4 0-.6-.1M66.9 35.2c0 1.3 1.1 2.4 2.4 2.4 1.3 0 2.4-1.1 2.4-2.4 0-1.3-1.1-2.4-2.4-2.4-1.3 0-2.4 1-2.4 2.4M59.8 34c-1.3.4-2.1 1.7-1.7 3 .4 1.3 1.7 2.1 3 1.7 1.3-.3 2.1-1.7 1.7-3-.3-1.1-1.3-1.8-2.4-1.8-.2 0-.4 0-.6.1M50.9 37.7c-1.2.7-1.6 2.2-.9 3.3.7 1.2 2.2 1.6 3.3.9 1.2-.7 1.6-2.1.9-3.3-.5-.8-1.3-1.2-2.1-1.2-.4-.1-.8.1-1.2.3M36.7 48.6c-1.2.7-1.6 2.2-.9 3.3.7 1.2 2.2 1.6 3.3.9 1.2-.7 1.6-2.1.9-3.3-.4-.8-1.3-1.2-2.1-1.2-.4 0-.9.1-1.2.3M29 51.8c-1.3.4-2.1 1.7-1.7 3 .4 1.3 1.7 2.1 3 1.7 1.3-.4 2.1-1.7 1.7-3-.3-1.1-1.3-1.8-2.4-1.8-.2 0-.4.1-.6.1M18.3 55.3c0 1.3 1.1 2.4 2.4 2.4 1.3 0 2.4-1.1 2.4-2.4 0-1.3-1.1-2.4-2.4-2.4-1.3 0-2.4 1.1-2.4 2.4M23.1 35.2c0 1.3-1.1 2.4-2.4 2.4-1.3 0-2.4-1.1-2.4-2.4 0-1.3 1.1-2.4 2.4-2.4 1.3 0 2.4 1 2.4 2.4M30.2 34c1.3.4 2.1 1.7 1.7 3-.4 1.3-1.7 2.1-3 1.7-1.3-.3-2.1-1.7-1.7-3 .3-1.1 1.3-1.8 2.4-1.8.2 0 .4 0 .6.1M39.1 37.7c1.2.7 1.6 2.2.9 3.3-.7 1.2-2.2 1.6-3.3.9-1.2-.7-1.6-2.1-.9-3.3.5-.8 1.3-1.2 2.1-1.2.4-.1.8.1 1.2.3M53.3 48.6c1.2.7 1.6 2.2.9 3.3-.7 1.2-2.2 1.6-3.3.9-1.2-.7-1.6-2.1-.9-3.3.4-.8 1.3-1.2 2.1-1.2.4 0 .9.1 1.2.3M61 51.8c1.3.4 2.1 1.7 1.7 3-.4 1.3-1.7 2.1-3 1.7-1.3-.4-2.1-1.7-1.7-3 .3-1.1 1.3-1.8 2.4-1.8.2 0 .4.1.6.1M71.7 55.3c0 1.3-1.1 2.4-2.4 2.4-1.3 0-2.4-1.1-2.4-2.4 0-1.3 1.1-2.4 2.4-2.4 1.3 0 2.4 1.1 2.4 2.4"/></svg>
                <div class="col-12">
                    <h2>Sistema d’aerotermia</h2>
                </div>
                <div class="col-lg-4 offset-1 about-aero xlargemi5">
                    <h3>Què és?</h3>
                    <p>La aerotermia és la manera ideal de tenir la temperatura perfecta a la teva llar independentment de la estació de l’any, sense sorpreses en la factura i d’una manera totalment sostenible i compromesa amb el medi ambient. És una tecnologia que utilitza principalment la energia de l’aire per climatitzar els espais i per produir aigua calenta sanitària.</p>
                    <h3 class="howit">Com funciona?</h3>
                    <p>El funcionament consisteix en l’aprofitament de l’energia continguda en l’aire de l’entorn. Una bomba de calor absorbeix i utilitza l’energia de l’aire de l’exterior i la transfereix al circuit de la calefacció de la vivenda i al sistema de producció d’aigua calenta sanitària. A més, refresca l’ambient a l’estiu.</p>
                    <img src="{{ asset('/legacy/svg/aerotermia.svg') }}" alt="aerotermia compte">
                    <div class="control">
                        <img src="{{ asset('/legacy/svg/control.svg') }}" class="control" alt="Control a distància">
                        <h3>Control a distància</h3>
                    <p>
                        El sistema de climatització està basat en el núvol i permet controlar de manera remota l’estat de cada unitat des de qualsevol dispositiu mòbil o PC a través de l’app MELCloud. L’aplicació permet gestionar i controlar de manera senzilla la temperatura dels dos circuits de calefacció i els paràmetres de l’aigua calenta sanitària. A més, inclou funcions bàsiques com la protecció anti-
congelació, el mode vacances i el temporitzador.</p>

                    </div>

                </div>
                <div class="col-lg-6 offset-lg-1 advantages">
                    <svg version="1.1" id="alogo" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 90 90" xml:space="preserve"><style>.st0{fill:#fff}</style><path d="M45 90c24.9 0 45-20.1 45-45S69.9 0 45 0 0 20.1 0 45s20.1 45 45 45"/><path class="st0" d="M44.4 47.6c-1.3-.4-2.1-1.7-1.7-3 .4-1.3 1.7-2.1 3-1.7 1.3.3 2.1 1.7 1.7 3-.3 1.1-1.3 1.8-2.4 1.8-.2 0-.4 0-.6-.1M66.9 35.2c0 1.3 1.1 2.4 2.4 2.4 1.3 0 2.4-1.1 2.4-2.4 0-1.3-1.1-2.4-2.4-2.4-1.3 0-2.4 1-2.4 2.4M59.8 34c-1.3.4-2.1 1.7-1.7 3 .4 1.3 1.7 2.1 3 1.7 1.3-.3 2.1-1.7 1.7-3-.3-1.1-1.3-1.8-2.4-1.8-.2 0-.4 0-.6.1M50.9 37.7c-1.2.7-1.6 2.2-.9 3.3.7 1.2 2.2 1.6 3.3.9 1.2-.7 1.6-2.1.9-3.3-.5-.8-1.3-1.2-2.1-1.2-.4-.1-.8.1-1.2.3M36.7 48.6c-1.2.7-1.6 2.2-.9 3.3.7 1.2 2.2 1.6 3.3.9 1.2-.7 1.6-2.1.9-3.3-.4-.8-1.3-1.2-2.1-1.2-.4 0-.9.1-1.2.3M29 51.8c-1.3.4-2.1 1.7-1.7 3 .4 1.3 1.7 2.1 3 1.7 1.3-.4 2.1-1.7 1.7-3-.3-1.1-1.3-1.8-2.4-1.8-.2 0-.4.1-.6.1M18.3 55.3c0 1.3 1.1 2.4 2.4 2.4 1.3 0 2.4-1.1 2.4-2.4 0-1.3-1.1-2.4-2.4-2.4-1.3 0-2.4 1.1-2.4 2.4M23.1 35.2c0 1.3-1.1 2.4-2.4 2.4-1.3 0-2.4-1.1-2.4-2.4 0-1.3 1.1-2.4 2.4-2.4 1.3 0 2.4 1 2.4 2.4M30.2 34c1.3.4 2.1 1.7 1.7 3-.4 1.3-1.7 2.1-3 1.7-1.3-.3-2.1-1.7-1.7-3 .3-1.1 1.3-1.8 2.4-1.8.2 0 .4 0 .6.1M39.1 37.7c1.2.7 1.6 2.2.9 3.3-.7 1.2-2.2 1.6-3.3.9-1.2-.7-1.6-2.1-.9-3.3.5-.8 1.3-1.2 2.1-1.2.4-.1.8.1 1.2.3M53.3 48.6c1.2.7 1.6 2.2.9 3.3-.7 1.2-2.2 1.6-3.3.9-1.2-.7-1.6-2.1-.9-3.3.4-.8 1.3-1.2 2.1-1.2.4 0 .9.1 1.2.3M61 51.8c1.3.4 2.1 1.7 1.7 3-.4 1.3-1.7 2.1-3 1.7-1.3-.4-2.1-1.7-1.7-3 .3-1.1 1.3-1.8 2.4-1.8.2 0 .4.1.6.1M71.7 55.3c0 1.3-1.1 2.4-2.4 2.4-1.3 0-2.4-1.1-2.4-2.4 0-1.3 1.1-2.4 2.4-2.4 1.3 0 2.4 1.1 2.4 2.4"/></svg>
                    <div class="container">
                        <div class="row percentage">
                            <div class="col-12">
                                <h3 class="r-title mb">Avantatges</h3>
                            </div>
                            <div class="col-4 offset-1 offset-xl-1 span-wrap first">
                                    <h4>Respecte al</h4>
                                    <p>Gasoil</p>
                                    <span>55%</span>
                            </div>
                            <div class="col-4 span-wrap">
                                    <h4>Respecte al</h4>
                                    <p>Gas natural</p>
                                    <span>35%</span>

                            </div>

                            <div class="col-4 col-xl-3">
                                <h3 class="r-title">Estalvi (%)</h3>
                            </div>
                        </div>
                        <div class="row percentage second">
                            <div class="col offset-1 col-lg-10">
                                <p>Estudi realitzat per Mitsubishi Electric basat en el sistema Ecodan de calefacció, refrigeració i ACS d'alta eficiencia que es basa en el principi de la bomba de calor aerotèrmica.</p>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-xl-11 offset-xl-1 consume first">
                            <h3 class="r-title">Consum energètic</h3>
                            </div>
                            <div class="col-11 offset-sm-1 offset-lg-0 offset-xl-1 consume">
                                <img src="{{ asset('/legacy/svg/consume.svg') }}" alt="Consum energètic">
                            </div>
                        </div>

                        <div class="row energy-footer">
                            <div class="col-12 col-sm-4 offset-sm-1 offset-lg-0 offset-xl-1">
                                <h4>Energia</h4>
                                <p><span>149</span> kWh/annum</p>
                            </div>
                            <div class="col-12 col-sm-6">
                                <h4>Intensitat del so</h4>
                                <p><span>42</span> dB</p>
                            </div>
                            <div class="col-11 offset-sm-1 offset-lg-0 offset-xl-1">
                                <p class="more-info">Per a més informació consulta els següents vídeos sobre el sistema d’aerotermia:</p>
                                <a href="https://vimeo.com/248286518" class="btn btn-primary" target="blank">Aerotermia 1<div class="img film"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488.3 488.3"><path d="M488.3 142.5v203.1c0 15.7-17 25.5-30.6 17.7l-84.6-48.8v13.9c0 41.8-33.9 75.7-75.7 75.7H75.7C33.9 404.1 0 370.2 0 328.4V159.9c0-41.8 33.9-75.7 75.7-75.7h221.8c41.8 0 75.7 33.9 75.7 75.7v13.9l84.6-48.8c13.5-8 30.5 1.9 30.5 17.5z"/></svg></div></a>
                                <a href="https://www.youtube.com/watch?v=rEwLVtMuK5k&feature=youtu.be" class="btn btn-primary" target="blank">Aerotermia 2<div class="img film"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488.3 488.3"><path d="M488.3 142.5v203.1c0 15.7-17 25.5-30.6 17.7l-84.6-48.8v13.9c0 41.8-33.9 75.7-75.7 75.7H75.7C33.9 404.1 0 370.2 0 328.4V159.9c0-41.8 33.9-75.7 75.7-75.7h221.8c41.8 0 75.7 33.9 75.7 75.7v13.9l84.6-48.8c13.5-8 30.5 1.9 30.5 17.5z"/></svg></div></a>
                                <a href="https://www.youtube.com/watch?v=BXA5ER5RaIE" class="btn btn-primary" target="blank">Aerotermia 3<div class="img film"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 488.3 488.3"><path d="M488.3 142.5v203.1c0 15.7-17 25.5-30.6 17.7l-84.6-48.8v13.9c0 41.8-33.9 75.7-75.7 75.7H75.7C33.9 404.1 0 370.2 0 328.4V159.9c0-41.8 33.9-75.7 75.7-75.7h221.8c41.8 0 75.7 33.9 75.7 75.7v13.9l84.6-48.8c13.5-8 30.5 1.9 30.5 17.5z"/></svg></div></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="best" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 offset-md-1 xlargehalf">
                    <h2>Amb les millors marques</h2>
                </div>

                <div class="col-12 col-md-6 memory">
                    <a id ="zoom" class="btn btn-primary zoom" href="files/memoria-qualitat-sert.pdf" role="button" target="blank">Memòria de qualitat <div class="img memory"></div></a>
                </div>
            </div>

            <div class="row brands first">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Edifici</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/exterior.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/default.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Construcció  de façana d’obra vista, estucat de color i alumini. Coronaments i ampits de pedra i/o prefabricats. Materials molt resistents, perdurables en el temps i que suposen molt poc manteniment.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Climatització</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/climatitzacio.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/mitsubishi.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>S’utilitza la tecnologia de l’aerotermia per climatitzar els espais, generant tant calor com fred, i per produir aigua calenta sanitària. S’instal·la el sistema de terra radiant en totes les habitacions.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="http://es.mitsubishielectric.com/es/" target="blank">Mitsubishi</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Tancaments exteriors</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/fusteria-exterior.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/default.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Bastiment exterior d’alumini amb trencament de pont tèrmic. Vidres Climalit amb doble cambra d’aire i amb control solar i baix emissiu. Persianes d’alumini motoritzades.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="https://climalit.es/" target="blank">climalit.es</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Tancaments interiors</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/fusteria.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/default.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Porta d’entrada cuirassada d’alta seguretat. Bastiment folrat amb fusta a escollir entre: roure, faig, cedre o lacat de color blanc. Portes interiors a escollir entre varies fustes: roure, faig, cedre o lacades de color blanc. Manetes de les portes d’acer inoxidable a escollir entre varis models. Vidres interiors a escollir entre diversos models.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Paviments</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/general.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/porcelanosa.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Terra general interior a escollir dins les següents opcions: A. Gres de la marca Porcelanosa o escollir fins 40u/m². B. Parquet models faig, roure, jatoba o similar de qualitat.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="http://www.porcelanosa.com/" target="blank">porcelanosa.com</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Terrassa</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/terrassa.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/porcelanosa.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Paviment de gres porcel·lànic exterior.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="http://www.porcelanosa.com/" target="blank">porcelanosa.com</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Cuina</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/cuina.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/dica.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Banc de cuina i frontals a escollir entre granet, Silestone o Dekton tipus blanc dune o similars. Mobles de rexapat de fusta o laca a escollir entre varis models i distribucions. Equipada amb forn, campana i vitro-ceràmica de la marca Siemens o similar categoria. Aigüera de la marca Franke de tectonite amb una cubeta de 50cm d’ample. Griferia de la marca Hansgrohe model Focus o similar.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="http://www.dica.es/" target="blank">dica.es</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Banys</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/banys.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/hansgrohe.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Rajola marca Porcelanosa a escollir fins 40u/m². Griferia Hansgrohe model Focus monomando i termostàtiques Ecostat. Plat de dutxa extraplà a nivel del terra. Mampares de vidre fetes a mida. Sanitaris marca villeroy O.Novo o similar. Mobles de bany de diseny model EASY  de la marca Mapini o similar. Equipats amb mirall.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="http://hansgrohe.es" target="blank">hansgrohe.es</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Armaris</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/armaris.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/default.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Armaris encastats equipats amb calaixos, prestatges i barres. Distribució interior a escollir. Interiors acabats amb melamina i exteriors a escollir entre varies fustes: roure, faig, cedre o lacades de color blanc. Tiradors d’acer inoxidable a escollir entre varis models.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Pintura</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/pintura.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="/legacy/svg/brands/default.svg" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Interior dels pisos acabat amb pintura plàstica en parets i sostres. Color a escollir.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row brands">
                <div class="col-12 col-md-11 offset-md-1 xlarge">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-2 xlargemi grif"><h3>Instal·lació elèctrica</h3></div>
                            <div class="col-6 col-sm-6 col-md-2 img"><img src="{{ asset('/legacy/img/brands/electrica.jpg') }}" class="img-fluid"></div>
                            <div class="col-6 col-sm-6 col-md-2 brand d-flex justify-content-center align-items-start"><img src="{{ asset('/legacy/svg/brands/simon.svg') }}" class="img-fluid"></div>
                            <div class="col-12 col-md-5 col-xl-4 text"><p>Interruptors i endolls de la sèrie Simon 82 de color blanc instal·lats segons planell d’instal·lació. Instal·lació de videoporter.</p></div>
                            <div class="col-12 col-md-3 col-lg-3 col-xl-2 link"><a href="https://www.simonelectric.com/coleccion-mecanismos/empotrar/simon-82" target="blank">simonelectric.com</a></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>

    <section id="localization" class="promo scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2>Localitzacions d’interès</h2>
                    <p>La promoció Josep Maria Sert es troba a la ciutat de Vic, a 5 minuts de la Plaça Major, en un entorn tranquil i privilegiat envoltat de zones verdes i amb una gran varietat de serveis que faran la vida més còmoda als seus propietaris. A poca distància podem trobar supermercats, centres educatius, centres de salut, l'hospital i una gran oferta de restauració i oci.</p>
                </div>
            </div>
        </div>
    </section>

<section id="gmaps" class="scroll-fx" data-sfx='{
                "downAnimation": "fadeIn",
                "downRepeat": true,
                "downDelay": "",
                "upAnimation": "",
                "upRepeat": true,
                "upDelay": ""
                }'>
    <div id="map-canvas" data-href="https://www.google.es/maps/place/Carrer+Josep+Maria+Sert,+08500+Vic,+Barcelona/@41.933226,2.2580305,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5270123c63a77:0x935b02d65cd506be!8m2!3d41.933226!4d2.2602192" style="height:780px;" ></div>
    </div>
</section>

@endsection