@extends('layout')
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
@endpush
@push('scripts')
    <script src="{{ mix('assets/js/app.js') }}" defer></script>
    <script src="{{ mix('assets/js/shared.js') }}" defer></script>
@endpush
