<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getlocale()) }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        {{ isset($title) ? __($title) . ' | ' : '' }} {{ 'compte.tv' }}
    </title>
    @stack('styles')
</head>

<?php $extractBodyClasses = 'route-action-' . strtolower(Route::current()->getActionMethod()) . ' route-name-' . Route::currentRouteName() . ' view-' . $view_name; ?>

<body
    data-route-name="{{ Route::currentRouteName() }}"
    data-route-action="{{ strtolower(Route::current()->getActionMethod()) }}"
    class="d-flex flex-column h-100 {{ $extractBodyClasses }} position-realtive"
    >

    <a name="top" id="top"></a>

    <main role="main" class="flex-shrink-0">
    @includeIf('components/header')
    <div id="@yield('wrapper-id')" class="@yield('wrapper-class')">
        @yield('page')
    </div>
    </main>
    
    <div class="position-relative mt-auto">
    @includeIf('components/footer')
    </div>

    @includeIf('components/aside-info')

    @stack('scripts')
</body>

</html>