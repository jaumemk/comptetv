(function($){

    // console.log("Base log!");
var slugs = {
    0:{
        slug:"/promocions/img/slide-01.svg"
    },
    1:{
        slug:"/promocions/img/slide-02.svg"
    },
    2:{
        slug:"/promocions/img/slide-03.svg"
    }

};
var flats = {
    0:{
        title: "Pis planta baixa 2",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "161,48m²",
        terrace: "73,01m²",
        download: "files/planta-baixa-2.pdf"
    },
    1:{
        title: "Pis planta baixa 3",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "3 habitacions",
        bath: "2",
        surface: "116,08m²",
        terrace: "13,07m²",
        download: "files/planta-baixa-3.pdf"
    },
    2:{
        title: "Pis planta baixa 4",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "3 habitacions",
        bath: "2",
        surface: "109,79m²",
        terrace: "14,12m²",
        download: "files/planta-baixa-4.pdf"
    },
    3:{
        title: "Pis 1-2",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "128,04m²",
        terrace: "169,53m²",
        download: "files/pis-primer-segona.pdf"
    },
    4:{
        title: "Pis 1-3",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "127,57m²",
        terrace: "13,37m²",
        download: "files/pis-primer-tercera.zip"
    },
    5:{
        title: "Pis 2-1",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "123,19m²",
        terrace: "7,24m²",
        download: "files/pis-segon-primera.pdf"
    },
    6:{
        title: "Pis 2-2",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "127,43m²",
        terrace: "11,03m²",
        download: "files/pis-segon-segona.pdf"
    },
    7:{
        title: "Pis 2-3",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "127,60m²",
        terrace: "11,03m²",
        download: "files/pis-segon-tercera.pdf"
    },
    8: {
        title: "Pis 3-1",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "121,40m²",
        terrace: "8,86m²",
        download: "files/pis-tercer-primera.pdf"
    },
    9: {
        title: "Pis 3-2",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "131,16m²",
        terrace: "11,03m²",
        download: "files/pis-tercer-segona.pdf"
    },
    10: {
        title: "Pis 4-1",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "123,43m²",
        terrace: "7,02m²",
        download: "files/pis-quart-primera.pdf"
    },
    11: {
        title: "Àtic",
        lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "137,82m²",
        terrace: "250,89m²",
        download: "files/atic.pdf"
    }

};

var flatsnadal = {
    0:{
        title: "Pis BE",
        lead: "Pisos i àtic de diverses dimensions, curosament acabats amb les millors marques. Disposen d’entre dues, tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 325m² i pàrquing.",
        rooms: "2 habitacions",
        bath: "1",
        surface: "75,29m²",
        terrace: "108,25m²",
        download: "files/be.pdf"
    },
    1:{
        title: "Pis 1A",
        lead: "Pisos i àtic de diverses dimensions, curosament acabats amb les millors marques. Disposen d’entre dues, tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 325m² i pàrquing.",
        rooms: "3 habitacions",
        bath: "2",
        surface: "106,41m²",
        terrace: "43,81m²",
        download: "files/1a.pdf"
    },
    2:{
        title: "Pis 2C",
        lead: "Pisos i àtic de diverses dimensions, curosament acabats amb les millors marques. Disposen d’entre dues, tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 325m² i pàrquing.",
        rooms: "4 habitacions",
        bath: "2",
        surface: "125,82m²",
        terrace: "15,88m²",
        download: "files/2c.pdf"
    }


};

$( document ).ready(function() {
    var d = new Date();
    var n = d.getFullYear();
    scrollFx.init({debug: false});
    preloadImages.init({debug: false});

    $('#theyear').text(n);
    $('.flats .title').html(flats[0]['title']);
    $('.flats .lead').html(flats[0]['lead']);
    $('.flats .rooms').html(flats[0]['rooms']);
    $('.flats .bath').html(flats[0]['bath']);
    $('.flats .surface').html(flats[0]['surface']);
    $('.flats .terrace').html(flats[0]['terrace']);
    $('#download').attr('href',flats[0]['download']);

    $('.flatsnadal .title').html(flatsnadal[0]['title']);
    $('.flatsnadal .lead').html(flatsnadal[0]['lead']);
    $('.flatsnadal .rooms').html(flatsnadal[0]['rooms']);
    $('.flatsnadal .bath').html(flatsnadal[0]['bath']);
    $('.flatsnadal .surface').html(flatsnadal[0]['surface']);
    $('.flatsnadal .terrace').html(flatsnadal[0]['terrace']);
    $('#download').attr('href',flatsnadal[0]['download']);

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
});

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if(scroll>=1000){
        $('#wrap-arrow').addClass('active');
    } else{
        $('#wrap-arrow').removeClass('active');

    }
});

    $('#carousel-splash,#carousel-nadal').slick({
        autoplay: true,
        autoplaySpeed: 6000,
        appendDots: $('#carousel-dots'),
        pauseOnHover: true,
        arrows: true,
        prevArrow: false,
        dots: true,
        infinite: true,
        speed: 500,
        fade: false,
        cssEase: 'cubic-bezier(0.19,1,.22,1)',
        adaptiveHeight: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    dots: true,
                    arrows: true,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    dots: true,
                    arrows: true,
                }
            },
        ]
    });


    $('#carousel-splash,#carousel-nadal').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var total = slick.slideCount;
            nextSlide = nextSlide+1;
             //console.log(nextSlide+ "/"+total);

            //$('#price img').attr('src',slugs[nextSlide-1]['slug']);
            $('#frac').text(nextSlide);
            $('#total').text(total);

    });


    $('#tip-slide,#nad-slide').slick({
        autoplay: true,
        autoplaySpeed: 6000,
        appendDots: $('#carousel-dots'),
        pauseOnHover: true,
        arrows: true,
        dots: true,
        infinite: true,
        speed: 500,
        fade: false,
        cssEase: 'cubic-bezier(0.19,1,.22,1)',
        adaptiveHeight: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    dots: true,
                    arrows: true,
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    dots: true,
                    arrows: true,
                }
            },
        ]
    });

   $('.zoomin').slickLightbox({
      itemSelector        : 'a',
      navigateByKeyboard  : true
    });
   $('#tip-slide,#nad-slide').slickLightbox({
      itemSelector        : 'a',
      navigateByKeyboard  : true
    });




        // title: "Pis de <br>4 habitacions",
        // lead: "Pisos i àtics de grans dimensions, curosament acabats amb les millors marques. Disposen d’entre tres i quatre habitacions, amb àmplies terrasses i jardins de fins a 150m² i pàrquing.",
        // rooms: "4 habitacions",
        // bath: "2",
        // surface: "112m²",
        // terrace: "18m²"


    $('#tip-slide').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        linkUrl = '/img/tipologies/tipologies-'+nextSlide+'.png';

        $('#zoom').attr('href',linkUrl);
        var attr = $('#download').attr('download');
        $('#download').attr('href',flats[nextSlide]['download']);
        // $('zoom-inner').attr('href',linkUrl);
        //console.log(nextSlide);
        $('.flats .title').html(flats[nextSlide]['title']);
        $('.flats .lead').html(flats[nextSlide]['lead']);
        $('.flats .rooms').html(flats[nextSlide]['rooms']);
        $('.flats .bath').html(flats[nextSlide]['bath']);
        $('.flats .surface').html(flats[nextSlide]['surface']);
        $('.flats .terrace').html(flats[nextSlide]['terrace']);
    });

    $('#nad-slide').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        linkUrl = '/img/tipologies/nadal/tipologies-'+nextSlide+'.png';

        $('#zoom').attr('href',linkUrl);
        var attr = $('#download').attr('download');
        $('#download').attr('href',flatsnadal[nextSlide]['download']);
        // $('zoom-inner').attr('href',linkUrl);
        //console.log(nextSlide);
        $('.flatsnadal .title').html(flatsnadal[nextSlide]['title']);
        $('.flatsnadal .lead').html(flatsnadal[nextSlide]['lead']);
        $('.flatsnadal .rooms').html(flatsnadal[nextSlide]['rooms']);
        $('.flatsnadal .bath').html(flatsnadal[nextSlide]['bath']);
        $('.flatsnadal .surface').html(flatsnadal[nextSlide]['surface']);
        $('.flatsnadal .terrace').html(flatsnadal[nextSlide]['terrace']);
    });


/**
    Scroll Top Arrow
**/

    $("#wrap-arrow").click(function() {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#totop").offset().top
        }, 1000);

    });

    var lastScrollTop = 0;
    $(window).scroll(function (event) {
        var scroll = $(window).scrollTop();
        if(scroll>=1000){
            $('#wrap-arrow').addClass('active');
        } else{
            $('#wrap-arrow').removeClass('active');

        }

           var st = $(this).scrollTop();
           if (st > lastScrollTop){
                $('#info-panel').addClass('active');
           } else {
                $('#info-panel').removeClass('active');
           }
           lastScrollTop = st;
    });


/**
    Google Maps
**/


var criba = $('body').attr('id');
       if(criba == 'nadal'){
        latit = 41.9312491;
        longit = 2.241095;
       } else if (criba =='sert' || criba == 'home'){
        latit = 41.933660;
        longit = 2.259890;
       }
 googleMap.init({
        init: {
            center: {
                lat: latit,
                lng: longit,
            },
            zoom: 15,
        },
        styles: [
          {
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#f5f5f5"
              }
            ]
          },
          {
            "elementType": "labels.icon",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "elementType": "labels.text.stroke",
            "stylers": [
              {
                "color": "#f5f5f5"
              }
            ]
          },
          {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#bdbdbd"
              }
            ]
          },
          {
            "featureType": "administrative.province",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "saturation": -80
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#ffffff"
              }
            ]
          },
          {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#757575"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#dadada"
              }
            ]
          },
          {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#616161"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "geometry.stroke",
            "stylers": [
              {
                "color": "#c9c9c9"
              },
              {
                "visibility": "on"
              }
            ]
          },
          {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          },
          {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#e5e5e5"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "stylers": [
              {
                "visibility": "on"
              }
            ]
          },
          {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#eeeeee"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
              {
                "color": "#c9c9c9"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
              {
                "color": "#9e9e9e"
              }
            ]
          }

        ],
        callback: function(){

            // console.log('googleMap callback!!!');

        }
    });

})(jQuery);
//# sourceMappingURL=main.js.map
