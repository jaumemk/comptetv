var scrollFx = {

    elements: [],
    defaults: {
        selector: '.scroll-fx',
        debug: false,
        dsfx: {
            downAnimation: "fadeIn",
            downRepeat: false,
            downDelay: 0,
            upAnimation: "fadeIn",
            upRepeat: false,
            upDelay: 0,
            forceTouch: false
        },
    },

    init: function(options){

        // create settings from defaults and user options
        scrollFx.settings = $.extend({}, scrollFx.defaults, options);

        // init elements
        scrollFx.getElements();
    },

    getElements: function(){

        $(scrollFx.settings.selector).each(function(i,elem){

            // get data attributes (sfx)
            var dsfx = scrollFx.getAttributes(elem);

            // if (modernizr) is no-touch or force animation in touch
            if($('html').hasClass('no-touch') || dsfx.forceTouch ){

                // init css style on forceTouch
                if(dsfx.forceTouch)
                    $(elem).addClass('forceTouch');

                // Create a new Waypoints inview object
                new Waypoint.Inview({
                    element: elem,
                    enter: function(direction) {

                        scrollFx.notify('Enter ' + direction);

                        if(direction == "down"){
                            scrollFx.animateDown(elem, dsfx);
                        }

                        if(direction == "up"){
                            scrollFx.animateUp(elem, dsfx);
                        }

                    },
                    exited: function(direction) {

                        scrollFx.notify('Exited ' + direction);

                        if(direction == "down"){
                            scrollFx.resetDown(elem, dsfx);
                        }

                        if(direction == "up"){
                            scrollFx.resetUp(elem, dsfx);
                        }
                    }
                });

            }
        });

    },

    // get data-sfx attributes
    getAttributes: function(elem){

        var dsfx = $.extend({}, scrollFx.settings.dsfx, $(elem).data('sfx'));

        scrollFx.notify(dsfx);

        return dsfx;
    },

    // console log
    notify: function(msg){
        if(scrollFx.settings.debug)
            console.log(msg);
    },

    // Animate element on direction down
    animateDown: function(elem, dsfx){

        scrollFx.notify('AnimationDown');

        // if never triggered, then execute animation at least once
        if( !$(elem).data('downTriggered')){
            scrollFx.animateElement(elem, dsfx.downAnimation, dsfx.downDelay);
        }

        // if should not repeat, then set data attribute triggered
        if(dsfx.downRepeat == false){
            $(elem).data('downTriggered', true);
        }

    },

    animateUp: function(elem, dsfx){

        scrollFx.notify('AnimationUp');

        // if never triggered, then execute animation at least once
        if( !$(elem).data('upTriggered')){
            scrollFx.animateElement(elem, dsfx.upAnimation, dsfx.upDelay);
        }

        // if should not repeat, then set data attribute triggered
        if(dsfx.upRepeat == false){
            $(elem).data('upTriggered', true);
        }

    },

    // animate element
    animateElement: function (elem, animation, delay) {

        //remove 'ms' and parse int base 10
        if(typeof delay == "string")
            delay = parseInt(delay.replace("ms", ""), 10);

        // set delay and animate
        setTimeout(function(){
            $(elem).addClass("animated " + animation);
        }, delay);

    },

    // reset down animations classes
    resetDown: function(elem, dsfx){

        scrollFx.notify('ResetDown');

        // if up animation never triggered
        if(!$(elem).data('upTriggered')){
            // remove animated
            $(elem).removeClass("animated");
            // reset up animation
            scrollFx.resetAnimation(elem, dsfx.upAnimation);
        }

        // reset down animation
        scrollFx.resetAnimation(elem, dsfx.downAnimation);
    },

    // reset up animations classes
    resetUp: function(elem, dsfx){

        scrollFx.notify('ResetUp');

        // if down animation never triggered
        if(!$(elem).data('downTriggered')){
            // remove animated
            $(elem).removeClass("animated");
            // reset down animation
            scrollFx.resetAnimation(elem, dsfx.downAnimation);
        }

        // reset up animation
        scrollFx.resetAnimation(elem, dsfx.upAnimation);
    },

    // reset animation class
    resetAnimation: function(elem, animation){
        $(elem).removeClass(animation);
    }

};
