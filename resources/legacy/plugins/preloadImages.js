var preloadImages = {

    defaults: {
        selector: 'img.preload',
        loadedClass: 'show',
        debug: false,
        options: {}
    },

    init: function(options){

        // create settings from defaults and user options
        preloadImages.settings = $.extend({}, preloadImages.defaults, options);

        // $('img.preload').parent('.img').css('background-color', '#cccccc');

        $(preloadImages.settings.selector).imagesLoaded(preloadImages.settings.options)

            .progress( function( instance, image ) {
                var result = image.isLoaded ? 'loaded' : 'broken';

                $(image.img).addClass(preloadImages.settings.loadedClass);

                preloadImages.notify( 'Image is ' + result + ' for ' + image.img.src );
            })

            .always( function( instance ) {
                preloadImages.notify('all "'+preloadImages.settings.selector+'" loaded');
            });

    },

    // console log
    notify: function(msg){
        if(preloadImages.settings.debug)
            console.log(msg);
    },

};
