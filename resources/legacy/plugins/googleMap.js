

var googleMap = {

    object: undefined,
    defaults: {
        init: {
            div: '#map-canvas',
            center: {
                lat: 41.933660,
                lng: 2.259890,
            },
            zoom: 16,
            disableDefaultUI: false,
            scrollwheel: false,
        },
        styles: [],
        callback: function(){},
    },

    init: function(options) {

        googleMap.settings = $.extend( true, googleMap.defaults, options );

        googleMap.loadGoolgeMaps();

    },

    loadGoolgeMaps: function() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://maps.googleapis.com/maps/api/js?' +
           'key=AIzaSyCfQoIeLCeCFzP3j9PRcKOEgIjjQecYaSQ&' +
           // 'sensor=true&' +
            'callback=googleMap.loadGMaps';
        document.body.appendChild(script);

    },

    loadGMaps: function() {

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = '/legacy/js/vendor/gmaps.min.js';

        // Attach handlers for all browsers
        var done = false;
        script.onload = script.onreadystatechange = function()
        {
            if( !done && ( !this.readyState
                        || this.readyState == "loaded"
                        || this.readyState == "complete") )
            {
                done = true;

                // Continue your code
                googleMap.initialize();

                // Handle memory leak in IE
                script.onload = script.onreadystatechange = null;
                document.body.removeChild( script );
            }
        };

        document.body.appendChild(script);

    },

    initialize: function(){

        //Init Map at center
       var map =  googleMap.object = new GMaps(googleMap.settings.init);

        //set base Styles
        googleMap.object.addStyle({
            styledMapName:"Google Map",
            styles: googleMap.settings.styles,
            mapTypeId: "default_style"
        });

        var criba = $('body').attr('id');

       if(criba == 'nadal'){
            var locations = [
              ['Nova promoció Nadal', 41.9312491, 2.241095, '/legacy/svg/marker.svg'],
              ['Farmàcia', 41.9332418, 2.2422366, '/legacy/svg/pharmacy.svg'],
              ['Institut Jaume Callís', 41.9338427, 2.2414179, '/legacy/svg/school.svg'],
              ['Vicdental S.L.', 41.9332032, 2.2430354, '/legacy/svg/bocodental.svg'],
              ['Club Tennis Vic', 41.9341492, 2.2329926, '/legacy/svg/sport.svg'],
              ['Universitat de Vic - Universitat Central de Catalunya', 41.9332921, 2.2474027, '/legacy/svg/school.svg'],
              ['Parc de Joan Riera i Rius', 41.9315884, 2.2407098, '/legacy/svg/park.svg'],
              ['Club Patí Vic', 41.9322014, 2.2395714, '/legacy/svg/sport.svg'],
              ['Escola Andersen', 41.9328135, 2.2331631, '/legacy/svg/school.svg'],
              ['Residència El Nadal', 41.9293594, 2.2392206, '/legacy/svg/residence.svg'],
              ['Club Atlètic Vic', 41.936053, 2.2370113, '/legacy/svg/sport.svg'],
              ['Pavelló Ausoneta', 41.9368342, 2.2356212, '/legacy/svg/sport.svg'],
              ['Estadi Municipal de Futbol', 41.9379749, 2.2352021, '/legacy/svg/sport.svg'],
              ];


            var contentstring =[
                ['<a href="https://www.google.es/maps/place/Carrer+del+Pare+Rambla,+08500+Vic,+Barcelona/@41.9320724,2.2388085,17z/data=!3m1!4b1!4m5!3m4!1s0x12a527223588a3b9:0xc7f40235e15092c3!8m2!3d41.9320724!4d2.2409972" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Farm%C3%A0cia+Anna+Pou/@41.9332279,2.2399922,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5272192b7f369:0xa577b7790a6e4a0d!8m2!3d41.9332279!4d2.2421809" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Institut+Jaume+Callis/@41.9333901,2.2392254,17z/data=!3m1!4b1!4m5!3m4!1s0x12a527219a43ba89:0xd5a1d07f2a8ccc50!8m2!3d41.9333901!4d2.2414141" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Vicdental+SL/@41.9332842,2.2408629,17z/data=!3m1!4b1!4m5!3m4!1s0x12a52721e45f6031:0xf5f69a249cb95d6!8m2!3d41.9332842!4d2.2430516?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Club+Tennis+Vic/@41.934327,2.2316233,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5272626a08577:0x3566f151f76a696a!8m2!3d41.934327!4d2.233812?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Universitat+de+Vic+-+Universitat+Central+de+Catalunya/@41.9337609,2.2450932,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5271e968781e5:0xe9183aa8308fbbc0!8m2!3d41.9337609!4d2.2472819?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Parc+de+Joan+Riera+i+Rius/@41.9316341,2.2384822,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5271d91e3450b:0xefa9210409da5e89!8m2!3d41.9316341!4d2.2406709?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Club+Patí+Vic/@41.9329534,2.2369777,17z/data=!3m1!4b1!4m5!3m4!1s0x12a527183e1f7e85:0x1be4954ec759329e!8m2!3d41.9329534!4d2.2391664?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Escola+Andersen/@41.9325203,2.2309274,17z/data=!4m5!3m4!1s0x12a5272f530416c1:0xef82584459a25f91!8m2!3d41.9325203!4d2.2331161?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Resid%C3%A8ncia+El+Nadal/@41.9297199,2.2372478,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5272373bf2779:0xe4bd29b3755dc531!8m2!3d41.9297199!4d2.2394365?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Club+Atl%C3%A8tic+Vic/@41.936053,2.2370113,17z/data=!3m1!4b1!4m5!3m4!1s0x12a52720c687e6ab:0xfcefdec0f652b049!8m2!3d41.936053!4d2.2392?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Pavell%C3%B3+Ausoneta/@41.9368342,2.2356212,17z/data=!3m1!4b1!4m5!3m4!1s0x12a52727463b7ff5:0xf6264ab8b2e545eb!8m2!3d41.9368342!4d2.2378099?hl=ca" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Estadi+Municipal+de+Futbol/@41.9379749,2.2352021,17z/data=!3m1!4b1!4m5!3m4!1s0x12a526d8a3c625ef:0x358b4f8fe1337510!8m2!3d41.9379749!4d2.2373908?hl=ca" target="blank">Maps</a>'],
            ];

        } else if (criba =='sert' || criba == 'home'){
            var locations = [
              ['Despatx de Compte', 41.933912, 2.253703, '/legacy/svg/cmark.svg'],
              ['Nova promoció Sert', 41.933051, 2.260490, '/legacy/svg/marker.svg'],
              ['Farmàcia', 41.93117333529875, 2.260308265686035, '/legacy/svg/pharmacy.svg'],
              ['Supermercat Supeco', 41.93020355173835, 2.26617693901062, '/legacy/svg/supermarket.svg'],
              ['Supermercat Consum', 41.932476, 2.259633, '/legacy/svg/supermarket.svg'],
              ['Supermercat Bonpreu', 41.93516804956578, 2.266509532928467, '/legacy/svg/supermarket.svg'],
              ['IES Vic', 41.93472908221202, 2.2589510679244995, '/legacy/svg/school.svg'],
              ['Col·legi Escorial', 41.92999602412308, 2.259589433670044, '/legacy/svg/school.svg'],
              ['Temple Romà', 41.9292157, 2.256882799999971, '/legacy/svg/temple.svg'],
              ['Cap Osona', 41.93109351827857, 2.2608715295791626, '/legacy/svg/hospital.svg'],
              ['Clínica Sant Josep', 41.929784504897384, 2.2607427835464478, '/legacy/svg/hospital.svg'],
              ['Hospital General de Vic', 41.936684458996154, 2.260286808013916, '/legacy/svg/hospital.svg'],
              ['Bayés Clínica', 41.93448166291764, 2.2562313079833984, '/legacy/svg/hospital.svg'],
              ['Plaça Major de Vic', 41.9304988783345, 2.2542786598205566, '/legacy/svg/square.svg'],
              ['Col·legi Sant Miquel dels Sants', 41.9330546, 2.2557429999999385, '/legacy/svg/school.svg'],
              ['Escola bressol Caputxins', 41.9351205, 2.2627072000000226, '/legacy/svg/school.svg'],
              ['Llar d\'Infants Vedruna' , 41.9326493, 2.263177799999994, '/legacy/svg/school.svg'],
              ];


            var contentstring =[
                ['<a href="https://www.google.com/maps/place/Ronda+de+Francesc+Camprodon,+13,+08500+Vic,+Barcelona/@41.9340979,2.2534405,17z/data=!3m1!4b1!4m8!1m2!2m1!1sCamprodon+13+entres%C3%B2l+segona+vic!3m4!1s0x12a527030802d779:0xd9786027358a991a!8m2!3d41.9340939!4d2.2556292" target="blank">Maps</a>'],
                ['<a href="https://www.google.es/maps/place/Carrer+Josep+Maria+Sert,+08500+Vic,+Barcelona/@41.933226,2.2580305,17z/data=!3m1!4b1!4m5!3m4!1s0x12a5270123c63a77:0x935b02d65cd506be!8m2!3d41.933226!4d2.2602192" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Farmacia+J.+Arum%C3%AD/@41.931473,2.259832,18.29z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a52706dbfe55c9:0x117932b328e5bc7f!8m2!3d41.9311718!4d2.2603123" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Supeco/@41.9308515,2.2647083,18z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a527a9b55d20b3:0x84abf72d879d8ab2!8m2!3d41.9302006!4d2.2661796" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Supermercados+Consum/@41.931473,2.259832,18.29z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a527012258ccaf:0x4e97083bd19d3fda!8m2!3d41.9323406!4d2.2598422" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Supermercado+Bonpreu/@41.9334754,2.2650459,17.99z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a527a976680ce9:0x89ffff7d539f986e!8m2!3d41.9351666!4d2.2665169" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Instituto+de+Vic/@41.9350896,2.2592643,17z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a527019ce6453d:0xc5341ee49210a28e!8m2!3d41.9347281!4d2.2589558" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Escorial/@41.9303631,2.2592716,17.99z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a52706f377594d:0x21102a1c024973c7!8m2!3d41.9299955!4d2.2595894" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Templo+Romano+de+VIC/@41.9295217,2.2559384,17.99z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a52705b7e2726d:0x1f0bc0b9fdd916e6!8m2!3d41.9292158!4d2.2568831" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/CAP+Osona/@41.9298599,2.2598398,17.99z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a527072220be4f:0x2c2476f86441502e!8m2!3d41.9310935!4d2.2608715" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Cl%C3%ADnica+Sant+Josep/@41.9298599,2.2598398,17.99z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x0:0xf9308d4c19cc1d2e!8m2!3d41.9297825!4d2.2607441" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Hospital+Universitari+de+Vic+(Consorci+Hospitalari+de+Vic)/@41.9359386,2.2551086,16.07z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a52701c15a818f:0x15227bd283e5ba40!8m2!3d41.9366745!4d2.2602908" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Bay%C3%A9s+Cl%C3%ADnica/@41.9359386,2.2551086,16.07z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x0:0x5e2d02dee1185d4c!8m2!3d41.9344797!4d2.256238" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Pla%C3%A7a+major+de+Vic/@41.9305177,2.2391767,14z/data=!4m13!1m7!3m6!1s0x12a5270123c63a77:0x935b02d65cd506be!2sCarrer+Josep+Maria+Sert,+08500+Vic,+Barcelona!3b1!8m2!3d41.933226!4d2.2602192!3m4!1s0x12a52704c2d1da11:0xa602e1521a95f88f!8m2!3d41.9304989!4d2.254284" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Col%C2%B7legi+Sant+Miquel+dels+Sants/@41.9324046,2.2565357,19z/data=!4m13!1m7!3m6!1s0x12a527040a06c9bd:0xe8940134aabebf57!2sCarrer+de+Jaume+I+el+Conqueridor,+08500+Vic,+Barcelona!3b1!8m2!3d41.9324046!4d2.2570882!3m4!1s0x12a527039d0a67c7:0x39dc9538952674ec!8m2!3d41.933055!4d2.2557431" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/EBMV+Capuchinos/@41.9351205,2.2605132,17z/data=!4m12!1m6!3m5!1s0x12a52700708ec5b3:0x9bf264d8f7a04f35!2sEBMV+Capuchinos!8m2!3d41.9351205!4d2.2627072!3m4!1s0x12a52700708ec5b3:0x9bf264d8f7a04f35!8m2!3d41.9351205!4d2.2627072" target="blank">Maps</a>'],
                ['<a href="https://www.google.com/maps/place/Carrer+Cam%C3%AD+de+l\'Escorial,+24,+08500+Vic,+Barcelona/@41.9326493,2.2609891,17z/data=!3m1!4b1!4m5!3m4!1s0x12a527aa07b14fdd:0xbacd304fb0279edf!8m2!3d41.9326493!4d2.2631778" target="blank">Maps</a>'],
                //['<a href="" target="blank">Maps</a>'],
            ];
        }



    for( i = 0; i < locations.length; i++ ) {

        var infowindow = new google.maps.InfoWindow({
            content: contentstring[i][0]
          });
        var marker =  googleMap.object.addMarker({
            lat: locations[i][1],
            lng: locations[i][2],
            icon: {
                url: locations[i][3],
                scaledSize: {
                    height: 40,
                    width: 30
                }
            },
        });


        // Add info window to marker
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i][0]+'<br>'+contentstring[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));

    }


        googleMap.object.setStyle("default_style");

        // // add default marker
        // if( googleMap.settings.marker.lat !== null && googleMap.settings.marker.lng !== null){
        //     googleMap.object.addMarker(googleMap.settings.marker);
        // }

        //fire callback function
        googleMap.settings.callback();

    }

};
