window._ = require('lodash');

try {
    //window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
    window.jQueryBridget = require('jquery-bridget');

    require('bootstrap');
    
    require('slick-carousel');
    window.AOS = require('aos');

    window.Isotope = require('isotope-layout');
    jQueryBridget('isotope', Isotope, $ );


} catch (e) {}

AOS.init({
    //once: true,
});