/*
    Projects page isotope behaviours
*/

//// filter
$('#slidedown-2 li a').on('click', function (e) {
    $('#slidedown-2 li a.active').removeClass('active');
    $(this).addClass('active');
    showClass = $(this).data('filter');
    $('#projects-container').isotope({
        filter: showClass
    });
});

//// on load isotope check if a filter should be active
$(window).on('load', function () {

    $('#projects-container').isotope({
        filter: '*',
    });

    if(window.location.hash){
        url = window.location.hash;
        filter = url.substring(url.indexOf("#") + 1);  
        $('#slidedown-2 li .filter-' + filter + '').trigger('click');
    }
});