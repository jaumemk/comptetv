require('./bootstrap');
require('./googleMap');
require('./projectsIsotope');

/*
    DOM Load
*/

$(window).on('load', function () {

    $('#project-slider').slick({
        mobileFirst: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 768, // medium
            settings: {
                dots: false,
                arrows: true,
            }
        }]
    });

    $('#last-projects-slider').slick({
        mobileFirst: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [{
                breakpoint: 768, // medium
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false,
                }
            },
            {
                breakpoint: 992, // large
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false,
                }
            },
            {
                breakpoint: 1200, // mxl
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: false,
                }
            }
        ]
    });

    // AOS needs refresh after slick initialized (why? AOS bug?)
    AOS.refresh();

    // Initialize map for google maps

    initMap();
});